// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package animal;

import scala.annotation.tailrec;

import scala.actors.Actor;
import scala.math.max;
import scala.math.min;

import dna.Dna;

object Direction extends Enumeration {
	val N, S, E, W = Value;
	
	def convert(value: Int): Direction.Value = value match {
		case 0 => N;
		case 1 => E;
		case 2 => S;
		case _ => W;
	}
	
	def convert(direction: Direction.Value): (Int, Int) = direction match {
		case N => (-1, 0);
		case S => (1, 0);
		case E => (0, 1);
		case W => (0, -1);
	}
}

trait CauseOfDeath;

case object Starvation extends CauseOfDeath;
case object OldAge extends CauseOfDeath;
case class Killed(killer: String) extends CauseOfDeath;

abstract class AnimalTracker {
	def onAct(action: Action): Unit;
	def onDie(cause: CauseOfDeath): Unit;
	def track(message: String): Unit;
}

object AnimalTrackerImpl extends AnimalTracker {
	def onAct(action: Action): Unit = Console.println(action.toString);
	def onDie(cause: CauseOfDeath): Unit = Console.println("death from " + cause + "\n");
	def track(message: String): Unit = Console.println(message);
}

trait AnimalProcess {
	def animal: Animal;
	def isAlive: Boolean = animal.alive;
	
	def getOlder[T >: this.type <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		animal.getOlder match {
			case None => ();
			case Some(cause) => die(cause, map, universe);
		}
	}
	
	def die[T >: this.type <: AnimalProcess](cause: CauseOfDeath, map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		map.tile(animal.position).get.animalProcess = None;
		animal.die(cause);
		map.death(this, cause, universe);
	}
	
	def performAction[T >: this.type <: AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		animal.performAction(action, time, map, universe) match { // This has to be done here because the map needs the AnimalProcess and not just the Animal
			case Some(newPosition: (Int, Int)) => {
				map.tile(animal.position).get.animalProcess = None;
				
				animal.position = newPosition;
				map.tile(newPosition).get.animalProcess = Some(this): Option[this.type];
			}
			
			case _ => ();
		}
	}
	
//		override def equals(o: Any) = super.equals(o)
//		override def hashCode = super.hashCode
}

trait AnimalSimple extends AnimalProcess {
	def act(time: Int, map: evolution.Map[AnimalSimple], universe: evolution.Universe[AnimalSimple]): Unit = {
		val action = animal.chooseAction(time, map);
		
		performAction(action, time, map, universe);
	}
}

class AnimalSimpleImp(val animal: Animal) extends AnimalSimple;

trait AnimalActor extends AnimalProcess with Actor {
	def act() = {
		loop {
			react {
				case evolution.ChooseAction(time, map) => {
					sender ! evolution.Choice(this, animal.chooseAction(time, map));
				}
				
				case evolution.Stop => exit();
			}
		}
	}
}

class AnimalActorImp(val animal: Animal) extends AnimalActor;

trait Animal {
	def alive: Boolean;
	def alive_=(arg: Boolean): Unit;

	def chooseAction[T <: AnimalProcess](time: Int, map: evolution.Map[T]): Action;
	def performAction[T <: AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Option[(Int, Int)];
	
	def getOlder: Option[CauseOfDeath];
	
	def die(cause: CauseOfDeath): Unit = {
		alive = false;
		
		if(!tracker.isEmpty) tracker.get.onDie(cause);
	}
	
	def tracker: Option[AnimalTracker];
	def tracker_=(arg: Option[AnimalTracker]): Unit;
	
	def dna: Dna;
	
	def perception[T <: AnimalProcess](time: Int, map: evolution.Map[T]): Array[Double];
	
	def age: Int;
	def nbChildren: Int;
	
	def position: (Int, Int);
	def position_=(pos: (Int, Int)): Unit;
	
	def healthPoints: Int;
	def healthPoints_=(hp: Int): Unit;
	
	val generation: Int;
	def ageLineage: Int;
}

object Species {
	def apply(animal: Animal): AnimalSpecies = animal match {
		case s: Sheep => SheepType;
		case w: Wolf => WolfType;
	}
}

trait AnimalSpecies;

case object WolfType extends AnimalSpecies {
	override def toString = "WolfType";
}

case object SheepType extends AnimalSpecies {
	override def toString = "SheepType";
}

trait Action;

trait ActionWolf extends Action;

case class WolfMove(val direction: Direction.Value) extends ActionWolf {
	override val toString: String = "WolfMove(" + direction + ")";
}

object WolfNothing extends ActionWolf {
	override val toString: String = "WolfNothing";
}

object WolfProcreate extends ActionWolf {
	override val toString: String = "WolfProcreate";
}

trait Wolf extends Animal {
	def age: Int;
	def food: Double;
	
	def reference: WolfReference;
	
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): Wolf;
}

trait WolfReference {
	def sightRange: Int;
	def infoPerTile: Int;
}

class WolfBasicReference (
	val lifeExpectancy: Int, // 200
	val energyLimit: Double, // 50
	val constantHungerRate: Double, // 1
	val computationalHungerRate: Double, // 0.0
	val sightRange: Int, // 2
	val maxHealthPoints: Int, // 100
	val strength: Int // 100
) extends WolfReference {
	val infoPerTile = 4; // Tile? Wolf? Sheep? Amount of grass
	val numberActions = 6; // 4 moves, nothing, procreation
	
	val perceptionVecSize: Int = infoPerTile * (2 * sightRange + 1) * (2 * sightRange + 1) + 1;
}

class WolfImpBasic(
	val reference: WolfBasicReference,
	var alive: Boolean,
	var tracker: Option[AnimalTracker],
	var age: Int,
	var energy: Double,
	var healthPoints: Int,
	var position: (Int, Int),
	val dna: Dna,
	val generation: Int,
	val ageLineage: Int
) extends Wolf {
	def this(reference: WolfBasicReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	def food: Double = energy;
	
	var nbChildren: Int =  0;
	
	def performAction[T<:AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Option[(Int, Int)] = {
		if(!tracker.isEmpty) {
			tracker.get.track("energy=" + energy + "; age=" + age);
			tracker.get.onAct(action);
	   	}
		
		action match {
			case move: WolfMove => {
				val targetPosition = map.addPosDir(Direction.convert(move.direction), position);
			
				map.tile(targetPosition) match {
					case None => { // Trying to move on a tile that doesn't exist
						if(!tracker.isEmpty) tracker.get.track("that's the edge of the world");
						
						None;
					}
				
					case Some(targetTile) => targetTile.animalProcess match {
						case None => { // Moving to an empty tile
							Some(targetPosition);
						}
					
						case Some(animalProcess) => { // Attacking another animal
							animalProcess.animal.healthPoints -= reference.strength;
							
							if(!tracker.isEmpty) tracker.get.track("attacked " + animalProcess.animal);
							
							if(animalProcess.animal.healthPoints <= 0) { // If the animal under attack is killed
								val food = animalProcess.animal match {
									case wolf: Wolf => {
										// TODO
										//val food = wolf.food / 2;
										val food = 2;
										
										if(!tracker.isEmpty) tracker.get.track("ate " + food + " food");
										
										food;
									}
						
									case sheep: Sheep => {
										val food = sheep.food;
									
										if(!tracker.isEmpty) tracker.get.track("ate " + food + " food");
										
										food;
									}
								}
								
								energy = min(reference.energyLimit, (energy + food));
						
								animalProcess.die(Killed("wolf"), map, universe);
								
								Some(targetPosition);
							}
							else { // If the animal under attack is not killed
								None;
							}
						}
					}
				}
			}
		
			case WolfProcreate => {
				procreate(map, universe);
				
				None;
			}
		
			case WolfNothing => None; // Nothing LOL
		
			case other => None; //Console.println("Not codded yet (" + other + ")");
		}
	}
	
	def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		map.neighbourhood(position, 1).find(_._2.animalProcess.isEmpty) match {
			case None => if(!tracker.isEmpty) tracker.get.track("no free tile to give birth"); // There is no free tile
		
			case Some((babyPos, _)) => {
				val food: Double = (energy / 2) - 1;
		
				energy = food;
		
				if(food > 0) {
					nbChildren += 1;
					
					val child = generateChild(scala.util.Random);
					child.energy = food;
					child.position = babyPos;
					
					map.birth(child, universe);
				}
				else if(!tracker.isEmpty) tracker.get.track("not enough food to procreate");
			}
		}
	}

	// Get the perception vector
	// TODO See the health points and the age of the different animals
	def perception[T <: AnimalProcess](time: Int, map: evolution.Map[T]): Array[Double] = {
		// The perception vector
		val perceptionVec = new Array[Double](reference.perceptionVecSize);
		
		var refId: Int = 0;
		var dx: Int = -reference.sightRange;
		while(dx <= reference.sightRange) {
			var dy = -reference.sightRange;
			while(dy <= reference.sightRange) {
				// Position of the tile we're looking at
				val px = position._1 + dx;
				val py = position._2 + dy;
	
				map.tile(px, py) match {
					case Some(tile) => {
						perceptionVec(refId + 0) = 1; // Is there a tile here? (it could be the edge of the world)
			
						tile.animalProcess match {
							case Some(aP) => aP.animal match {
								case _: Wolf => {
									perceptionVec(refId + 1) = 1; // Is there a wolf here?
								}
						
								case _: Sheep => {
									perceptionVec(refId + 2) = 1; // Is there a sheep here?
								}
							}
							
							case _ => ();
						}
		
						perceptionVec(refId + 3) = tile.grass.update(time).toDouble / evolution.Grass.maxHeight; // What amout of grass is there?
					}
					
					case None => ();
				}
				
				refId += reference.infoPerTile;
				
				dy += 1;
			}
			
			dx += 1;
		}
		
		assert(refId == (perceptionVec.size - 1), refId + " != " + (perceptionVec.size - 1));
		
		// Energy
		perceptionVec(refId) = (energy / reference.energyLimit);
		
		perceptionVec;
	}
	
	// Choose an action
	def chooseAction[T <: AnimalProcess](time: Int, map: evolution.Map[T]): ActionWolf = {
		val perceptionVec = perception(time, map);
		
		val actionId = neuralNetwork.Math.selectFromProb(dna.interact(perceptionVec));
		
		if(actionId < 4) WolfMove(Direction.convert(actionId));
		else if(actionId == 4) WolfNothing;
		else WolfProcreate;
	}
	
	// Generate a child
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): WolfImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new WolfImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new WolfImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
	
	def getOlder: Option[CauseOfDeath] = {
		age += 1;
		energy -= reference.constantHungerRate + (reference.computationalHungerRate * dna.norm2);
		
		if(age > reference.lifeExpectancy) Some(OldAge);
		else if(energy <= 0.0) Some(Starvation);
		else None;
	}
}

class WolfGrowUpReference(
	lifeExpectancy: Int, // 200
	energyLimit: Double, // 50
	constantHungerRate: Double, // 1
	computationalHungerRate: Double, // 0.0
	sightRange: Int, // 2
	maxHealthPoints: Int, // 100
	strength: Int, // 100,
	val adulthood: (Int, Int) // (20, 160)
) extends WolfBasicReference(lifeExpectancy, energyLimit, constantHungerRate, computationalHungerRate, sightRange, maxHealthPoints, strength) {
	override def toString: String = "WolfGrowUpReference(lifeExpectancy=" + lifeExpectancy + ", energyLimit=" + energyLimit + ", constantHungerRate=" + constantHungerRate + ", computationalHungerRate=" + computationalHungerRate + ", sightRange=" + sightRange + ", maxHealthPoints=" + maxHealthPoints + ", strength=" + strength + ", adulthood=" + adulthood + ")";
}

// Very similar to WolfImpBasic, the only difference is they can have babies only during a certain period of time
class WolfImpGrowUp(
	reference: WolfGrowUpReference,
	alive0: Boolean, // Scala makes constructor parameters directly available to the methods in the class, so if I want to use a mutable parameter in one of this class's methods, I need to change the name of this parameter
	tracker0: Option[AnimalTracker], 
	age0: Int,
	energy0: Double,
	healthPoints0: Int,
	position0: (Int, Int),
	dna: Dna,
	generation: Int,
	ageLineage: Int
) extends WolfImpBasic(reference, alive0, tracker0, age0, energy0, healthPoints0, position0, dna, generation, ageLineage) {
	def this(reference: WolfGrowUpReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	// Is the wolf adult?
	def isAdult: Boolean = (age >= reference.adulthood._1) && (age < reference.adulthood._2);
	
	override def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		if(!isAdult) {
			if(!tracker.isEmpty) tracker.get.track("not in age of procreation");
			return;
		}
		
		super.procreate(map, universe);
	}
	
	// Generate a child
	override def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): WolfImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new WolfImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new WolfImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
}

trait ActionSheep extends Action;

case class SheepMove(val direction: Direction.Value) extends ActionSheep {
	override val toString: String = "SheepMove(" + direction + ")";
}

object SheepEat extends ActionSheep {
	override val toString: String = "SheepEat";
}

object SheepProcreate extends ActionSheep {
	override val toString: String = "SheepProcreate";
}

trait Sheep extends Animal {
	def age: Int;
	def food: Double;
	
	def reference: SheepReference;
	
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): Sheep;
}

trait SheepReference {
	def sightRange: Int;
	def infoPerTile: Int;
}

class SheepBasicReference(
	val lifeExpectancy: Int, // 100
	val energyLimit: Double, // 60
	val constantHungerRate: Double, // 4
	val computationalHungerRate: Double, // 0.0
	val sightRange: Int, // 2
	val maxHealthPoints: Int, // 100
	val strength: Int // 35
) extends SheepReference {
	val infoPerTile = 4; // Tile? Wolf? Sheep? Amount of grass
	val numberActions = 6; // 4 moves, eat, procreation
	
	val perceptionVecSize = infoPerTile * (2 * sightRange + 1) * (2 * sightRange + 1) + 1;
}

class SheepImpBasic(
	val reference: SheepBasicReference,
	var alive: Boolean,
	var tracker: Option[AnimalTracker],
	var age: Int,
	var energy: Double,
	var healthPoints: Int,
	var position: (Int, Int),
	val dna: Dna,
	val generation: Int,
	val ageLineage: Int
) extends Sheep {
	def this(reference: SheepBasicReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	def food: Double = energy;
	
	var nbChildren: Int =  0;
	
	def performAction[T<:AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Option[(Int, Int)] = {
		if(!tracker.isEmpty) {
			tracker.get.track("energy=" + energy + "; age=" + age);
			tracker.get.onAct(action);
	   	}
		
		action match {
			case move: SheepMove => {
				val targetPosition = map.addPosDir(Direction.convert(move.direction), position);

				map.tile(targetPosition) match {
					case None => {
						if(!tracker.isEmpty) tracker.get.track("that's the edge of the world");
						
						None;
					}
			
					case Some(targetTile) => targetTile.animalProcess match {
						case None => {
							Some(targetPosition);
						}
				
						case Some(animalProcess) => {
							animalProcess.animal.healthPoints -= reference.strength;
							
							if(!tracker.isEmpty) tracker.get.track("attacked " + animalProcess.animal);
							
							if(animalProcess.animal.healthPoints <= 0) {
								animalProcess.die(Killed("wolf"), map, universe);
								
								Some(targetPosition);
							}
							else {
								None;
							}
						}
					}
				}
			}
		
			case SheepProcreate => {
				procreate(map, universe);
				
				None;
			}
		
			case SheepEat => {
				val grass = map.tile(position).get.grass;
				val grassQty = min((reference.energyLimit - energy).toInt, grass.update(time));
				
				grass.cut(grassQty);
				energy += grassQty;
				
				if(!tracker.isEmpty) tracker.get.track("ate " + grassQty + " unit(s) of grass");
				
				None;
			}
		
			case other => None;//Console.println("Not codded yet (" + other + ")");
		}
	}
	
	def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		map.neighbourhood(position, 1).find(_._2.animalProcess.isEmpty) match {
			case None => if(!tracker.isEmpty) tracker.get.track("no free tile to give birth"); // There is no free tile
		
			case Some((babyPos, _)) => {
				val food: Double = (energy / 2) - 1;
		
				energy = food;
		
				if(food > 0) {
					nbChildren += 1;
					
					
					val child = generateChild(scala.util.Random);
					child.energy = food;
					child.position = babyPos;
					
					map.birth(child, universe);
				}
				else if(!tracker.isEmpty) tracker.get.track("not enough food to procreate");
			}
		}
	}
	
	// Get the perception vector
	def perception[T <: AnimalProcess](time: Int, map: evolution.Map[T]): Array[Double] = {	
		// The perception vector
		val perceptionVec = new Array[Double](reference.perceptionVecSize);
		
		var refId: Int = 0;
		var dx: Int = -reference.sightRange;
		while(dx <= reference.sightRange) {
			var dy = -reference.sightRange;
			while(dy <= reference.sightRange) {
				// Position of the tile we're looking at
				val px = position._1 + dx;
				val py = position._2 + dy;
	
				map.tile(px, py) match {
					case Some(tile) => {
						perceptionVec(refId + 0) = 1; // Is there a tile here? (it could be the edge of the world)
			
						tile.animalProcess match {
							case Some(aP) => aP.animal match {
								case _: Wolf => {
									perceptionVec(refId + 1) = 1; // Is there a wolf here?
								}
						
								case _: Sheep => {
									perceptionVec(refId + 2) = 1; // Is there a sheep here?
								}
							}
							
							case _ => ();
						}
		
						perceptionVec(refId + 3) = tile.grass.update(time).toDouble / evolution.Grass.maxHeight; // What amout of grass is there?
					}
					
					case None => ();
				}
				
				refId += reference.infoPerTile;
				
				dy += 1;
			}
			
			dx += 1;
		}
		
		assert(refId == (perceptionVec.size - 1), refId + " != " + (perceptionVec.size - 1));
		
		// Energy
		perceptionVec(refId) = (energy / reference.energyLimit);
		
		perceptionVec;
	}
	
	// Choose an action
	def chooseAction[T <: AnimalProcess](time: Int, map: evolution.Map[T]): ActionSheep = {
		val perceptionVec = perception(time, map);
		
		val actionId = neuralNetwork.Math.selectFromProb(dna.interact(perceptionVec));
		
		if(actionId < 4) SheepMove(Direction.convert(actionId));
		else if(actionId == 4) SheepEat;
		else SheepProcreate;
	}
	
	// Generate a child
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): SheepImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new SheepImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new SheepImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
	
	def getOlder: Option[CauseOfDeath] = {
		age += 1;
		energy -= reference.constantHungerRate + (reference.computationalHungerRate * dna.norm2);
		
		if(age > reference.lifeExpectancy) Some(OldAge);
		else if(energy <= 0.0) Some(Starvation);
		else None;
	}
}

class SheepGrowUpReference(
	lifeExpectancy: Int, // 100
	energyLimit: Double, // 60
	constantHungerRate: Double, // 4
	computationalHungerRate: Double, // 0.0
	sightRange: Int, // 2
	maxHealthPoints: Int, // 100
	strength: Int, // 35
	val adulthood: (Int, Int) // (10, 80)
) extends SheepBasicReference(lifeExpectancy, energyLimit, constantHungerRate, computationalHungerRate, sightRange, maxHealthPoints, strength) {
	override def toString: String = "SheepGrowUpReference(lifeExpectancy=" + lifeExpectancy + ", energyLimit=" + energyLimit + ", constantHungerRate=" + constantHungerRate + ", computationalHungerRate=" + computationalHungerRate + ", sightRange=" + sightRange + ", maxHealthPoints=" + maxHealthPoints + ", strength=" + strength + ", adulthood=" + adulthood + ")";
}

// Very similar to SheepImpBasic, the only difference is they can have babies only during a certain period of time
class SheepImpGrowUp(
	reference: SheepGrowUpReference,
	alive0: Boolean,
	tracker0: Option[AnimalTracker],
	age0: Int,
	energy0: Double,
	healthPoints0: Int,
	position0: (Int, Int),
	dna: Dna,
	generation: Int,
	ageLineage: Int
) extends SheepImpBasic(reference, alive0, tracker0, age0, energy0, healthPoints0, position0, dna, generation, ageLineage) {
	def this(reference: SheepGrowUpReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	// Is the sheep adult?
	def isAdult: Boolean = (age >= reference.adulthood._1) && (age < reference.adulthood._2);
	
	override def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		if(!isAdult) {
			if(!tracker.isEmpty) tracker.get.track("not in age of procreation");
			return;
		}
		
		super.procreate(map, universe);
	}
	
	// Generate a child
	override def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): SheepImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new SheepImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new SheepImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
}
