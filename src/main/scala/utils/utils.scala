package utils;

import scala.annotation.tailrec;

object Maths {
	// Jensen-Shannon divergence
	// Between 0.0 and 1.0
	def JSD(p: Array[Double], q: Array[Double]): Double = {
		val length = p.length;
		
		require(length == q.length, length + " != " + q.length);
		
		var res: Double = 0.0;
		
		var i = 0;
		while(i < length) {
			val tmp = (p(i) + q(i)) / 2.0;
			
			// Small values are neglected
			if(p(i) > 1E-4) res += (p(i) * math.log(p(i) / tmp));
			if(q(i) > 1E-4) res += (q(i) * math.log(q(i) / tmp));
			
			i += 1;
		}
		
		(res / (2.0 * math.log(2.0)));
	}
	
	def variation(x: Double, y: Double, rate: Double): Boolean = {
		if(x == 0.0 && y != 0.0) true;
		else if(y == 0.0 && y == 0.0) true;
		else {
			val tmp = (x / y) - 1.0;
			
			(tmp >= rate || tmp <= -rate);
		}
	}
	
	// Input of the form (sum, count)
	@inline
	def average(x: (Int, Int)): Double = if(x._2 != 0) (x._1 / x._2.toDouble) else 0.0;
	
	@inline
	def average(x: Double, y: Int): Double = if(y != 0) (x / y) else 0.0;
	
	@inline
	def add(a: (Int, Int), b: (Int, Int)): (Int, Int) = (a._1 + b._1, a._2 + b._2);
	
	def product[T](l: List[List[T]]): List[List[T]] = l match {
		case Nil => List(Nil);
		case h :: t => for(xh <- h; xt <- product(t)) yield xh :: xt;
	}
	
	def bool2int(b: Boolean) = if(b) 1 else 0;
}

