// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dna;

import scala.annotation.tailrec;

import neuralNetwork.Math;

object Utils {
	def parseDnaStr(dnaStr: String, perceptionVecSize: Int, nbActions: Int, mutationFactor: Double): (() => Dna) = {
		@tailrec
		def analyseNN(layerStrs: List[String], inSize: Int, result: List[() => neuralNetwork.Layer], finalF: String, finalSize: Option[Int]): (List[() => neuralNetwork.Layer], Int) = layerStrs match {
			case Nil => ???;
			
			// Last layer
			case layerStr :: Nil => {
				val layerOpts = layerStr.split("-");

				val outSize = finalSize.getOrElse(layerOpts(1).toInt);
				val layerOptions = Array(layerOpts(0), finalF) ++ layerOpts.tail;
			val newLayerGen: (() => neuralNetwork.Layer) = LayerFactory.layerGenerator(inSize, outSize, layerOptions, Some(0.5));

//				val newLayerGen: (scala.util.Random => NNLayer) = layerOpts(0) match {
//					case "mat" => (randGen => {
//						val param = neuralNetwork.Array2DLayerParam();
//						neuralNetwork.Array2DLayer(, f)
//					});
//					
//					NNLayerMatrix(randGen, outSize, inSize, f, mutationFactor));
//					case _ => ???
////					case "mat" => (randGen => NNLayerMatrix(randGen, outSize, inSize, f, mutationFactor));
////					case "3dt" | _ => (randGen => NNLayer3DTensor(randGen, outSize, inSize, f, mutationFactor));
//				}
				
				((newLayerGen :: result).reverse, outSize);
			}
		
			case layerStr :: t => {
				val layerOpts = layerStr.split("-");

				val outSize = layerOpts(1).toInt;
				
				val layerOptions = Array(layerOpts(0), "ReLU") ++ layerOpts.tail;
				val newLayerGen: (() => neuralNetwork.Layer) = LayerFactory.layerGenerator(inSize, outSize, layerOptions, Some(0.5));
				
//				val f: (Array[Double] => Array[Double]) = utils.Maths.sigmoid;

//				val newLayerGen: (scala.util.Random => NNLayer) = layerOpts(0) match {
//					case "mat" => (randGen => NNLayerMatrix(randGen, outSize, inSize, f, mutationFactor));
//					case "3dt" | _ => (randGen => NNLayer3DTensor(randGen, outSize, inSize, f, mutationFactor));
//				}
				
				analyseNN(t, outSize, newLayerGen :: result, finalF, finalSize);
			}
		}
		
		val (dnaType, dnaArg) = dnaStr.split("--", 2) match {
			case tmp => (tmp(0), tmp(1));
		}
		
		dnaType match {
//			case "rnn" => {
//				val stepStrs = dnaArg.split("-!-", 3);
//				
//				val initLayerStrs = stepStrs(0).split("--").toList;
//				val (initLayerGens, outSizeInit) = analyseNN(initLayerStrs, perceptionVecSize, Nil, utils.Maths.sigmoid, None);
//				
//				val mainStep = stepStrs(1).split("-", 2);
//				val memorySize = mainStep(0).toInt;
//				val mainLayerStrs = mainStep(1).split("--").toList;
//				val mainLayerGens = analyseNN(mainLayerStrs, memorySize + outSizeInit, Nil, utils.Maths.sigmoid, Some(memorySize))._1;
//				
//				val finalLayerStrs = stepStrs(2).split("--").toList;
//				val finalLayerGens = analyseNN(finalLayerStrs, memorySize, Nil, utils.Maths.softMax, Some(nbActions))._1;
//				
//				(randGen: scala.util.Random) => new DnaRNNImp(initLayerGens.map(layerGen => layerGen(randGen)), mainLayerGens.map(layerGen => layerGen(randGen)), finalLayerGens.map(layerGen => layerGen(randGen)), new Array[Double](memorySize));
//			}
			
			case "adaptative" => {
				val coreLayers = Array[(neuralNetwork.Layer, Double)]();
				
				val layerB1: (neuralNetwork.Layer, Double) = { // TODO Ça pourrait être cool d'avoir des layers internes sparses
					val m = Array.ofDim[Double](nbActions, perceptionVecSize);
					val t = Array.ofDim[Double](nbActions);

					(neuralNetwork.Array2DLayer(m, t, Some(neuralNetwork.ReLU)), 0.0);
				}
			
				val layerB2: neuralNetwork.Layer = {
					val m = Array.ofDim[Double](nbActions, nbActions);
					val t = Array.ofDim[Double](nbActions);

					neuralNetwork.Array2DLayer(m, t, None);
				}
				
				val coeff: Double = 0.0;
				
				val adaptationFactor: Double = dnaArg.toDouble;
				
				() => {
					val layerA: neuralNetwork.Layer = {
						val m = Array.fill(nbActions, perceptionVecSize)(2 * (util.Random.nextDouble - 0.5));
						val t = Array.fill(nbActions)(2 * (util.Random.nextDouble - 0.5));

						neuralNetwork.Array2DLayer(m, t, None);
					}
					
					new AdaptativeDna(coreLayers, layerA, layerB1, layerB2, coeff, mutationFactor, adaptationFactor);
				}
			}
			
			case "nn" | _ => {
				val layerStrs = dnaArg.split("--").toList;
				
				val layerGens: List[(() => neuralNetwork.Layer)] = analyseNN(layerStrs, perceptionVecSize, Nil, "SoftMax", Some(nbActions))._1;
				
				() => new DnaNNImp(layerGens.map(layerGen => layerGen()), mutationFactor);
			}
		}
	}
}

object LayerFactory extends neuralNetwork.NNFactory;

trait Dna {
	def interact(perceptionVec: Array[Double]): Array[Double];
	def mutate(randGen: scala.util.Random): Dna;
	
	def stats: (Double, Double, Int); // sum and max (in abs) + number of params
	def norm2: Double;
}

object Mutator {
	def mutate(mutationFactor: Double, randGen: scala.util.Random)(layer: neuralNetwork.Layer): neuralNetwork.Layer = layer match {
		case layer: neuralNetwork.Array2DLayer => {
			val lM: Array[Array[Double]] = layer.p.m;
			val lT: Array[Double] = layer.p.t;
			
			val m = Array.ofDim[Array[Double]](lM.length);
			{var i = 0;
			while(i < lM.length) {
				val line = lM(i);
				val tmp = Array.ofDim[Double](line.length);
				
				var j = 0;
				while(j < line.length) {
					tmp(j) = line(j) + mutationFactor * (randGen.nextDouble - 0.5);
					
					j += 1;
				}
				
				m(i) = tmp;
				
				i += 1;
			}}
			
			val t = Array.ofDim[Double](lT.length);
			{var i = 0;
			while(i < lT.length) {
				t(i) = lT(i) + mutationFactor * (randGen.nextDouble - 0.5);
				
				i += 1;
			}}
			
			neuralNetwork.Array2DLayer(m, t, layer.nonLinearity);
		}
		
		case _ => ???;
	}
	
	def superMutate(mutationFactor: Double, randGen: scala.util.Random)(layer: neuralNetwork.Layer, modifCol: Int, modifLine: Int): neuralNetwork.Layer = layer match {
		case layer: neuralNetwork.Array2DLayer => {
			val lM: Array[Array[Double]] = layer.p.m;
			val lT: Array[Double] = layer.p.t;
			
			val height = layer.p.height + modifLine;
			val width = layer.p.width + modifCol;
			
			val m = Array.ofDim[Array[Double]](height);
			{val iMax: Int = if(modifLine >= 0) lM.length;
			else (lM.length + modifLine);
			
			var i = 0;
			while(i < iMax) {
				val line = lM(i);
				val tmp = Array.ofDim[Double](width);
				
				val jMax: Int = if(modifCol >= 0) line.length; // The additional coefficients are initially left at 0.0
				else width;
				
				var j = 0;
				while(j < jMax) {
					tmp(j) = line(j) + mutationFactor * (randGen.nextDouble - 0.5);
					
					j += 1;
				}
				
				m(i) = tmp;
				
				i += 1;
			}
			
			while(i < height) {
				m(i) = Array.ofDim[Double](width);
				
				i += 1;
			}}
			
			val t = Array.ofDim[Double](height);
			{val iMax: Int = if(modifLine >= 0) lT.length; // The additional coefficients are initially left at 0.0
			else height;
			
			var i = 0;
			while(i < iMax) {
				t(i) = lT(i) + mutationFactor * (randGen.nextDouble - 0.5);
				
				i += 1;
			}}
			
			neuralNetwork.Array2DLayer(m, t, layer.nonLinearity);
		}
		
		case _ => ???;
	}
}

trait DnaNN extends Dna {
	def layers: List[neuralNetwork.Layer];
	
	def interact(perceptionVec: Array[Double]): Array[Double] = layers.foldLeft(perceptionVec)((v: Array[Double], layer: neuralNetwork.Layer) => layer.apply(v));
	
	override def toString: String = "DnaNN(" + layers + ")";
}

class DnaNNImp(val layers: List[neuralNetwork.Layer], val mutationFactor: Double) extends DnaNN {
	def mutate(randGen: scala.util.Random): DnaNN = new DnaNNImp(layers.map(Mutator.mutate(mutationFactor, randGen)(_)), mutationFactor);
	
	val stats: (Double, Double, Int) = {
		var sum = 0.0;
		var max = 0.0;
		var nb = 0;
		
		for(layer <- layers) {
			val tmp = layer.p.stats;
			
			sum += tmp._1;
			if(tmp._2 > max) max = tmp._2;
			nb += tmp._3;
		}
		
		(sum, max, nb);
	}
	
	val norm2: Double = math.sqrt(layers.foldLeft(0.0)((acc: Double, layer: neuralNetwork.Layer) => acc + layer.p.norm2Sq));
}

class AdaptativeDna(
	val coreLayers: Array[(neuralNetwork.Layer, Double)], 
	val layerA: neuralNetwork.Layer, 
	val layerB1: (neuralNetwork.Layer, Double), 
	val layerB2: neuralNetwork.Layer, 
	val coeff: Double, // Used to determine when to add/remove a layer
	val mutationFactor: Double, 
	adaptationFactor: Double // Used to change coeff
) extends Dna {
	def interact(perceptionVec: Array[Double]): Array[Double] = {
		var vec = perceptionVec;
		{var i = 0;
		while(i < coreLayers.length) {
			vec = coreLayers(i)._1.apply(vec);
			
			i += 1;
		}}
		
		val outA = layerA.apply(vec);
		val outB = layerB2.apply(layerB1._1.apply(vec));
		
		Math.addTo(outA, outB);
		
		return Math.softMax(outA);
	}
	
	def mutate(randGen: scala.util.Random): AdaptativeDna = {
		val tmpCoeff = coeff + adaptationFactor * (randGen.nextDouble - 0.5);
		
		val (newCoeff, modifLayer): (Double, Int) = if(tmpCoeff >= 1.0) (tmpCoeff - 1, 1); // add a layer
		else if(tmpCoeff < 0.0) {
			if(coreLayers.length > 0) (tmpCoeff + 1, -1); // remove a layer
			else (0.0, 0);
		}
		else (tmpCoeff, 0);
		
		var modifCol: Int = 0;
		
		val newCoreLayers = Array.ofDim[(neuralNetwork.Layer, Double)](coreLayers.length + modifLayer);
		{val iMax: Int = if(modifLayer >= 0) coreLayers.length;
		else (coreLayers.length + modifLayer);
		
		var i = 0;
		while(i < iMax) {
			val (layer, k) = coreLayers(i);
			
			val tmpK = k + adaptationFactor * (randGen.nextDouble - 0.5);
			
			val (newK, modifLine): (Double, Int) = if(tmpK >= 1.0) (tmpK - 1 , 1); // add a line
			else if(tmpK < 0.0) {
				if(layer.p.height > 1) (tmpK + 1, -1); // remove a line
				else (0.0, 0);
			}
			else (tmpK, 0);
			
			val newLayer = Mutator.superMutate(mutationFactor, randGen)(layer, modifCol, modifLine);
			
			modifCol = modifLine; // If you add (resp. remove) a line to a layer, you have to add (resp. remove) a column to the following one
			
			newCoreLayers(i) = (newLayer, newK);
			
			i += 1;
		}}
		
		if(modifLayer >= 0) {
			val (modifColB, tmpLayer): (Int, (neuralNetwork.Layer, Double)) = {
				val (layer, k) = layerB1;
			
				var tmpK = k + adaptationFactor * (randGen.nextDouble - 0.5);
			
				val (newK, modifLine): (Double, Int) = if(tmpK >= 1.0) (tmpK - 1, 1); // add a line
				else if(tmpK < 0.0) {
					if(layer.p.height > 1) (tmpK + 1, -1); // remove a line
					else (0.0, 0);
				}
				else (tmpK, 0);
			
				val newLayer = Mutator.superMutate(mutationFactor, randGen)(layer, modifCol, modifLine);
			
				(modifLine, (newLayer, newK));
			}
			
			if(modifLayer == 0) {
				val newLayerA: neuralNetwork.Layer = Mutator.superMutate(mutationFactor, randGen)(layerA, modifCol, 0);
				val newLayerB1: (neuralNetwork.Layer, Double) = tmpLayer;
				
				val newLayerB2: neuralNetwork.Layer = Mutator.superMutate(mutationFactor, randGen)(layerB2, modifColB, 0);
				
				new AdaptativeDna(newCoreLayers, newLayerA, newLayerB1, newLayerB2, newCoeff, mutationFactor, adaptationFactor);
			}
			else {
				assert(modifLayer == 1, modifLayer + " != 1");
				
				newCoreLayers(coreLayers.length) = tmpLayer;
				
				val newLayerA: neuralNetwork.Layer = Mutator.superMutate(mutationFactor, randGen)(layerB2, modifColB, 0);
				
				val newLayerB1: (neuralNetwork.Layer, Double) = {
					val m = Array.ofDim[Double](layerB2.p.height, (layerB2.p.width + modifColB));
					val t = Array.ofDim[Double](layerB2.p.height);
	
					(neuralNetwork.Array2DLayer(m, t, Some(neuralNetwork.ReLU)), 0.5);
				}
				
				val newLayerB2: neuralNetwork.Layer = {
					val m = Array.ofDim[Double](layerB2.p.height, layerB2.p.height);
					val t = Array.ofDim[Double](layerB2.p.height);
	
					neuralNetwork.Array2DLayer(m, t, None);
				}
				
				new AdaptativeDna(newCoreLayers, newLayerA, newLayerB1, newLayerB2, newCoeff, mutationFactor, adaptationFactor);
			}
		}
		else { // A layer is removed
				val newLayerA: neuralNetwork.Layer = {
					val m = Array.ofDim[Double](layerA.p.height, (coreLayers.last._1.p.width + modifCol));
					val t = Array.ofDim[Double](layerA.p.height);
	
					neuralNetwork.Array2DLayer(m, t, None);
				}
				
				val newLayerB1: (neuralNetwork.Layer, Double) = {
					val (layer, k) = coreLayers.last;
			
					val tmpK = k + adaptationFactor * (randGen.nextDouble - 0.5);
			
					val (newK, modifLine): (Double, Int) = if(tmpK >= 1.0) (tmpK - 1 , 1); // add a line
					else if(tmpK < 0.0) {
						if(layer.p.height > 1) (tmpK + 1, -1); // remove a line
						else (0.0, 0);
					}
					else (tmpK, 0);
			
					val newLayer = Mutator.superMutate(mutationFactor, randGen)(layer, modifCol, modifLine);
					
					modifCol = modifLine;
					
					(newLayer, newK);
				}
				
				val newLayerB2: neuralNetwork.Layer = Mutator.superMutate(mutationFactor, randGen)(layerA, modifCol, 0);
				
				new AdaptativeDna(newCoreLayers, newLayerA, newLayerB1, newLayerB2, newCoeff, mutationFactor, adaptationFactor);
		}
	}
	
	val stats: (Double, Double, Int) = {
		var sum = 0.0;
		var max = 0.0;
		var nb = 0;
		
		@inline
		def aux(tmp: (Double, Double, Int)): Unit = {
			sum += tmp._1;
			if(tmp._2 > max) max = tmp._2;
			nb += tmp._3;
		}
		
		for(layer <- coreLayers) aux(layer._1.p.stats);
		
		aux(layerA.p.stats);
		aux(layerB1._1.p.stats);
		aux(layerB2.p.stats);
		
		(sum, max, nb);
	}
	
	val norm2: Double = {
		var res: Double = 0.0;
		
		{var i = 0;
		while(i < coreLayers.length) {
			res += coreLayers(i)._1.p.norm2Sq;
			
			i += 1;
		}}
		
		res += layerA.p.norm2Sq + layerB1._1.p.norm2Sq + layerB2.p.norm2Sq;
		
		math.sqrt(res);
	}
	
	override def toString: String = "AdaptativeDna([" + coreLayers.map(_._1).mkString(", ") + "], (" + layerA + " | " + layerB1._1 + ", " + layerB2 + "))";
}

//trait DnaNN extends Dna {
//    def layers: Seq[NNLayer];
//    
//    def interact(perceptionVec: Array[Double]): Array[Double] = {
//        layers.foldLeft(perceptionVec)((vec, layer) => layer.process(vec));
//    }
//    
//    def description: String = "DnaNN(" + layers.map(_.description).mkString(", ") + ")";
//}

//class DnaNNImp(val layers: Seq[NNLayer]) extends DnaNN {
//    def mutate(randGen: scala.util.Random): DnaNNImp = {
//        new DnaNNImp(layers.map(_.mutate(randGen)));
//    }
//}

//trait DnaRNN extends Dna {
//	def initLayers: Seq[NNLayer];
//	def mainLayers: Seq[NNLayer];
//	def finalLayers: Seq[NNLayer];
//	
//	def currentMemory: Array[Double];
//	def currentMemory_=(vec: Array[Double]): Unit;
//    
//    def interact(perceptionVec: Array[Double]): Array[Double] = {
//        val v = initLayers.foldLeft(perceptionVec)((vec, layer) => layer.process(vec));
//        
//        currentMemory = mainLayers.foldLeft(v ++ currentMemory)((vec, layer) => layer.process(vec));
//        
//        finalLayers.foldLeft(currentMemory)((vec, layer) => layer.process(vec));
//    }
//    
//    def description: String = "DnaRNN(" + initLayers.map(_.description).mkString(", ") + "; "  + mainLayers.map(_.description).mkString(", ") + "; "  + finalLayers.map(_.description).mkString(", ") + ")";
//}

//class DnaRNNImp(val initLayers: Seq[NNLayer], val mainLayers: Seq[NNLayer], val finalLayers: Seq[NNLayer], var currentMemory: Array[Double]) extends DnaRNN {
//    def mutate(randGen: scala.util.Random): DnaRNNImp = {
//        new DnaRNNImp(initLayers.map(_.mutate(randGen)), mainLayers.map(_.mutate(randGen)), finalLayers.map(_.mutate(randGen)), currentMemory.map(_ => 0.0));
//    }
//}

//trait NNLayer {
//	def process(vec: Array[Double]): Array[Double];
//	def mutate(randGen: scala.util.Random): NNLayer;
//	
//	def description: String;
//}

//object NNLayerMatrix {
//	def apply(randGen: scala.util.Random, numberActions: Int, vecSize: Int, f: (Array[Double] => Array[Double]), mutationFactor: Double): NNLayerMatrix = {
//		val matrixLayer = Array.ofDim[Double](numberActions, vecSize);
//		
//		for(i <- 0 to (numberActions - 1)) {
//			for(j <- 0 to (vecSize - 1)) {
//				matrixLayer(i)(j) = randGen.nextDouble * 2 - 1;
//			}
//		}
//		
//		new NNLayerMatrix(matrixLayer, f, mutationFactor);
//	}
//}

//class NNLayerMatrix(val data: Array[Array[Double]], val f: (Array[Double] => Array[Double]), val mutationFactor: Double) extends NNLayer {
//	def process(vec: Array[Double]): Array[Double] = f(utils.Maths.multiplySeq(data, vec));
//	
//	def mutate(randGen: scala.util.Random): NNLayerMatrix = new NNLayerMatrix(data.map(x => x.map(y => y + mutationFactor * (randGen.nextDouble - 0.5))), f, mutationFactor);
//	
//	override def toString: String = {
//	    val formatter = new java.text.DecimalFormat("#.###");
//	    
//	    data.map(line => line.map(x => formatter.format(x)).mkString(" \t")).mkString("\n");
//	}
//	
//	def description: String = "NNLayerMatrix(" + data.length + ", " + data(0).length + ")";
//}

//object NNLayer3DTensor {
//	def apply(randGen: scala.util.Random, numberActions: Int, vecSize: Int, f: (Array[Double] => Array[Double]), mutationFactor: Double): NNLayer3DTensor = {
//		val tensorLayer = Array.ofDim[Double](numberActions, vecSize, vecSize);
//		
//		for(i <- 0 to (numberActions - 1)) {
//			for(j <- 0 to (vecSize - 1)) {
//				for(k <- 0 to (vecSize - 1)) {
//					tensorLayer(i)(j)(k) = randGen.nextDouble * 2 - 1;
//				}
//			}
//		}
//		
//		new NNLayer3DTensor(tensorLayer, f, mutationFactor);
//	}
//}

//class NNLayer3DTensor(val data: Array[Array[Array[Double]]], val f: (Array[Double] => Array[Double]), val mutationFactor: Double) extends NNLayer {
//	def process(vec: Array[Double]): Array[Double] = f(utils.Maths.multiplySeqSeq(data, vec));
//	
//	def mutate(randGen: scala.util.Random): NNLayer3DTensor = new NNLayer3DTensor(data.map(x => x.map(y => y.map(z => z + mutationFactor * (randGen.nextDouble - 0.5)))), f, mutationFactor);
//	
//	override def toString: String = {
//	    val formatter = new java.text.DecimalFormat("#.###");
//	    
//	    data.map(matrix => matrix.map(line => line.map(x => formatter.format(x)).mkString(" \t")).mkString("\n")).mkString("\n- - - - - -\n");
//	}
//	
//	def description: String = "NNLayer3DTensor(" + data.length + ", " + data(0).length + ", " + data(0)(0).length + ")";
//}
