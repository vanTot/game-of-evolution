// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralGas;

import scala.annotation.tailrec;
import scala.collection.immutable;
import scala.collection.mutable;

trait NeuralGas {
	def neurons: Seq[Neuron];
	def adaptationStepSize: Double;
	def neighborhoodRange: Double;
	
	def adaptationStep(position: Array[Double], coeff: Double = 1.0): Unit = {
		for((neuron, i) <- neurons.sortBy(n => EuclideanSpace.dst(position, n.position)).view.zipWithIndex) { // I use view because of http://stackoverflow.com/questions/6833501/efficient-iteration-with-index-in-scala
			neuron.position = EuclideanSpace.+(neuron.position, EuclideanSpace.*(EuclideanSpace.-(position, neuron.position), (coeff * adaptationStepSize * math.exp(- i / neighborhoodRange))));
		}
	}
}

trait Neuron {
	def position: Array[Double];
	
	def position_=(newPosition: Array[Double]): Unit;
	
	override def toString: String = position.map("%.3f".format(_)).mkString("; ");
}

class NeuralGasImp(var neurons: Seq[Neuron], val adaptationStepSize: Double, val neighborhoodRange: Double) extends NeuralGas;

class NeuronImp(var position: Array[Double]) extends Neuron {
	def this(x: Double, y: Double) = this(Array(x, y));
}

object TestNeuralGas {
	def main(args: Array[String]): Unit = {
		val nbIterations = if(args.length > 0) args(0).toInt else 1000;
		val adaptationStepSize = if(args.length > 1) args(1).toDouble else 0.1;
		val neighborhoodRange = if(args.length > 2) args(2).toDouble else 1.0;
		
		val neurons = List.tabulate(10)(_ => new NeuronImp(scala.util.Random.nextDouble, scala.util.Random.nextDouble));
		
		val gas = new NeuralGasImp(neurons, adaptationStepSize, neighborhoodRange);
		
		Console.println("Before:");
		for(neuron <- gas.neurons) {
			val r = math.hypot(neuron.position(0), neuron.position(1));
			Console.println(r + " " + neuron);
		}
		
		for(iteration <- (0 to nbIterations)) {
			val theta = scala.util.Random.nextDouble * 2.0 * math.Pi;
			val p = Array(math.cos(theta), math.sin(theta));
			
			val r = math.hypot(p(0), p(1));
			
			gas.adaptationStep(p, 1.0);
		}
		
		Console.println("After:");
		for(neuron <- gas.neurons) {
			val r = math.hypot(neuron.position(0), neuron.position(1));
			Console.println(r + " " + neuron);
		}
	}
}

trait NormedVectorSpace[T, U] { // Kind of
	def +(v1: T, v2: T): T;
	def -(v1: T, v2: T): T;
	def *(v: T, k: U): T;
	def norm(v: T): Double;
	
	def dst(v1: T, v2: T): Double = norm(this.-(v1, v2));
}

object EuclideanSpace extends NormedVectorSpace[Array[Double], Double] {
	def +(v1: Array[Double], v2: Array[Double]): Array[Double] = {
		val l = v1.length;
		
		val res = Array.ofDim[Double](l);
		
		var row = 0;
		while(row < l) {
			res(row) = v1(row) + v2(row);
			
			row += 1;
		}
		
		res;
	}
	
	def -(v1: Array[Double], v2: Array[Double]): Array[Double] = {
		val l = v1.length;
		
		val res = Array.ofDim[Double](l);
		
		var row = 0;
		while(row < l) {
			res(row) = v1(row) - v2(row);
			
			row += 1;
		}
		
		res;
	}
	
	def *(v: Array[Double], k: Double): Array[Double] = {
		val l = v.length;
		
		val res = Array.ofDim[Double](l);
		
		var row = 0;
		while(row < l) {
			res(row) = v(row) * k;
			
			row += 1;
		}
		
		res;
	}
	
	def norm(v: Array[Double]): Double = {
		val l = v.length;
		
		var res: Double = 0.0;
		
		var row = 0;
		while(row < l) {
			res += math.pow(v(row), 2);
			
			row += 1;
		}
		
		math.sqrt(res);
	}
}
