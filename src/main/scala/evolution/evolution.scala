// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// For simple remote actor in Scala: http://stackoverflow.com/questions/4340277/how-do-i-disconnect-a-scala-remote-actor -> only with a version of Scala with Akka

package evolution;

import scala.actors.Actor;
import scala.actors.Actor._;
import scala.annotation.tailrec;
import scala.collection.immutable;
import scala.collection.mutable;
import io.Source;
import scala.math.ceil;
import scala.math.exp;
import scala.math.log;
import scala.math.max;
import scala.math.min;
import scala.math.pow;
import scala.math.sqrt;
import scala.util.Try;

import animal._;
import utils._;

object Evolution {
	def main(args: Array[String]): Unit = {
		java.util.Locale.setDefault(java.util.Locale.US);
		
		val randGen: scala.util.Random = scala.util.Random;
		
		val argumentsNames: immutable.Set[String] = immutable.Set[String](
			"configFile",
			"mapLength",
			"mapHeight",
			"nbProc",
			"nbIter",
			"sheepDna",
			"wolfDna",
			"timeManager",
			"logDir",
			"sheepA",
			"wolfA"
		);
		
		val arguments: mutable.Map[String, String] = Main.parseArguments(argumentsNames, args);
		
		// Read the configuration file if one is given as argument
		arguments.get("configFile") match {
			case Some(fileName) => {
				arguments ++= Source.fromFile(fileName).getLines.map(_.split("\t", 2)).map(x => (x(0), x(1)));
			}
			
			case None => ();
		}
		
		val mapLength: Int = Try(arguments("mapLength").toInt).toOption match {
			case None => 128;
			case Some(value) => value;
		}
		
		Console.println("mapLength = " + mapLength);
		
		val mapHeight: Int = Try(arguments("mapHeight").toInt).toOption match {
			case None => 128;
			case Some(value) => value;
		}
		
		Console.println("mapHeight = " + mapHeight);
		
		val nbProc: Int = Try(arguments("nbProc").toInt).toOption match {
			case None => 4;
			case Some(value) => value;
		}
		
		Console.println("nbProc = " + nbProc);
		
		val sheepFactory: (((Int, Int)) => Sheep) = Main.loadSheepFactory(arguments);
		
		val wolfFactory: (((Int, Int)) => Wolf) = Main.loadWolfFactory(arguments);
		
		// TODO So far only the universe stops after nbIter, and not the server
		// This is because the server listen to the input from the user
		val nbIter: Int = Try(arguments("nbIter").toInt).toOption match {
			case None => -1; // ~ never
			case Some(value) => value;
		}
		
		val timeManagerOpt: Option[TimeManager] = Try(arguments("timeManager")).toOption match {
			case Some(str) => {
				val info = str.split("-", 2);
				
				info(0) match {
					case "ratio" => Some(new ComputationRatio(info(1).toDouble));
					case "fixedDuration" => Some(new FixedTurnDuration(info(1).toInt));
					case "none" | _ => None;
				}
			}
			
			case _ => None;
		}
			
		val loggerOpt = Main.loadLogger(arguments);
		
		val universe = if(nbProc < 2) {
			def processFactory(animal: Animal): AnimalSimple = {
				new AnimalSimpleImp(animal);
			}
		
			// Generate the map
			val map = new MapImp[AnimalSimple](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile[AnimalSimple](None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalSimple](), processFactory, wolfFactory, sheepFactory, 0, 0);
			
			new UniverseSeqLogger(map, false, nbIter, false, timeManagerOpt, loggerOpt);
		}
		else {
			def processFactory(animal: Animal): AnimalActor = {
				new AnimalActorImp(animal);
			}
		
			// Generate the map
			val map = new MapImp[AnimalActor](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile[AnimalActor](None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalActor](), processFactory, wolfFactory, sheepFactory, 0, 0);
			
			new UniverseParLogger(map, false, nbIter, false, nbProc, timeManagerOpt, loggerOpt);
		}
		
		Console.println("universe = " + universe);
		
		val server = new LocalServer(universe);
		server.start;
	}
}

object Main {
	def main(args: Array[String]): Unit = {
		Console.println("Welcome to Evolution!\n");
		
		if(args.length > 0) {
			args(0) match {
				case "benchmark" => {
					Benchmark.main(args.drop(1));	
				}
				
				case "-h" | "-help" | "--help" => {
					Console.println("benchmark | evolution");
				}
				
				case "evolution" => {
					Evolution.main(args.drop(1));
				}
				
				case _ => {
					Evolution.main(args);
				}
			}
		}
		else {
			Evolution.main(args);
		}
	}
	
	def loadLogger(arguments: mutable.Map[String, String]): Option[Logger] = {
		var master: Option[LoggerModule] = None;
		val slaves = mutable.ListBuffer[LoggerModule]();
		
		arguments.get("ActLogger") match {
			case Some(str) => {
				val opts = str.split("\t");
				val module = new ActionsModule((opts(0)), opts(1).toInt);
				
				if(opts.length >= 3 && master == None) master = Some(module);
				else slaves += module;
			}
			
			case _ => ();
		}
		
		arguments.get("BehavLogger") match {
			case Some(str) => {
				val opts = str.split("\t");
				val module = new BehaviourModule((opts(0)), opts(1).toInt);
				
				if(opts.length >= 3 && master == None) master = Some(module);
				else slaves += module;
			}
			
			case _ => ();
		}
		
		arguments.get("DeathLogger") match {
			case Some(str) => {
				val opts = str.split("\t");
				val module = new DeathModule((opts(0)), opts(1).toInt);
				
				if(opts.length >= 3 && master == None) master = Some(module);
				else slaves += module;
			}
			
			case _ => ();
		}
		
		arguments.get("DemLogger") match {
			case Some(str) => {
				val opts = str.split("\t");
				val module = new DemographicModule((opts(0)), opts(1).toInt);
				
				if(opts.length >= 3 && master == None) master = Some(module);
				else slaves += module;
			}
			
			case _ => ();
		}
		
//		arguments.get("PosLogger") match {
//			case Some(str) => {
//				val opts = str.split("\t");
//				modules += new PositionModule((opts(0)), opts(1).toInt);
//			}
//			
//			case _ => ();
//		}
		
		master match {
			case Some(module) => Some(new ModularLogger(module, slaves.toList));
			case None => slaves.toList match {
				case h :: t => Some(new ModularLogger(h, t));
				case Nil => None;
			}	
		}
	}
	
	def loadSheepFactory(arguments: mutable.Map[String, String]): (((Int, Int)) => Sheep) = {
		val sheepDna: String = Try(arguments("sheepDna")).toOption match {
			case Some(str) => str;
			case _ => "nn--2DL";
		}
	
		val sheepReference: SheepGrowUpReference = {
			val lifeExpectancy: Int = Try(arguments("sheepLE").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val energyLimit: Double = Try(arguments("sheepEL").toDouble).toOption match {
				case Some(value) => value;
				case _ => 60.0;
			}
		
			val constantHungerRate: Double = Try(arguments("sheepConstHR").toDouble).toOption match {
				case Some(value) => value;
				case _ => 4.0;
			}
		
			val computationalHungerRate: Double = Try(arguments("sheepCompHR").toDouble).toOption match {
				case Some(value) => value;
				case _ => 0.0;
			}
		
			val sightRange: Int = Try(arguments("sheepSR").toInt).toOption match {
				case Some(value) => value;
				case _ => 2;
			}
		
			val maxHealthPoints: Int = Try(arguments("sheepHP").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val strength: Int = Try(arguments("sheepS").toInt).toOption match {
				case Some(value) => value;
				case _ => 35;
			}
		
			val adulthood: (Int, Int) = Try(arguments("sheepA").split("-").map(_.toInt)).toOption match {
				case Some(value) if value.length == 2 => (value(0), value(1));
				case _ => (10, 80);
			}
		
			new SheepGrowUpReference(lifeExpectancy, energyLimit, constantHungerRate, computationalHungerRate, sightRange, maxHealthPoints, strength, adulthood);
		}
		
		val dnaGenerator = dna.Utils.parseDnaStr(sheepDna, sheepReference.perceptionVecSize, sheepReference.numberActions, 0.1);
		
		Console.println("Sheep:");
		Console.println(sheepReference);
		Console.println(dnaGenerator());
	
		((position: (Int, Int)) => new SheepImpGrowUp(sheepReference, position, dnaGenerator(), 0, 0)); // Generates babies (age = 0)
	}
	
	def loadWolfFactory(arguments: mutable.Map[String, String]): (((Int, Int)) => Wolf) = {
		val wolfDna: String = Try(arguments("wolfDna")).toOption match {
			case Some(str) => str;
			case _ => "nn--2DL";
		}
		
		val wolfReference: WolfGrowUpReference = {
			val lifeExpectancy: Int = Try(arguments("wolfLE").toInt).toOption match {
				case Some(value) => value;
				case _ => 200;
			}
		
			val energyLimit: Double = Try(arguments("wolfEL").toDouble).toOption match {
				case Some(value) => value;
				case _ => 50.0;
			}
		
			val constantHungerRate: Double = Try(arguments("wolfConstHR").toDouble).toOption match {
				case Some(value) => value;
				case _ => 1.0;
			}
		
			val computationalHungerRate: Double = Try(arguments("wolfCompHR").toDouble).toOption match {
				case Some(value) => value;
				case _ => 0.0;
			}
		
			val sightRange: Int = Try(arguments("wolfSR").toInt).toOption match {
				case Some(value) => value;
				case _ => 2;
			}
		
			val maxHealthPoints: Int = Try(arguments("wolfHP").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val strength: Int = Try(arguments("wolfS").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val adulthood: (Int, Int) = Try(arguments("wolfA").split("-").map(_.toInt)).toOption match {
				case Some(value) if value.length == 2 => (value(0), value(1));
				case _ => (20, 160);
			}
		
			new WolfGrowUpReference(lifeExpectancy, energyLimit, constantHungerRate, computationalHungerRate, sightRange, maxHealthPoints, strength, adulthood); // Generates babies (age = 0)
		}
	
		val dnaGenerator = dna.Utils.parseDnaStr(wolfDna, wolfReference.perceptionVecSize, wolfReference.numberActions, 0.1);
		
		Console.println("Wolves:");
		Console.println(wolfReference);
		Console.println(dnaGenerator());
		
		((position: (Int, Int)) => new WolfImpGrowUp(wolfReference, position, dnaGenerator(), 0, 0));
	}
	
	// Arguments parser
	def parseArguments(argumentsNames: immutable.Set[String], args: Array[String]): mutable.Map[String, String] = {
		val arguments = mutable.HashMap[String, String]();
		
		{var i = 0;
			while(i < args.length) {
				if(args(i).startsWith("--")) {
					val argName = args(i).substring(2);
					
					if(argumentsNames(argName)) {
						arguments += ((argName, args(i + 1)));
						i += 1;
					}
					else {
						Console.println("Argument \"" + args(i) + "\" unknown.");
					}
				}
				else {
					Console.println("Why \"" + args(i) + "\"?");
				}
				
				i += 1;
			}
		}
		
		arguments;
	}
}
	
case object Stop;
case object Compute;
case object StartPrinting;
case object StopPrinting;
case object SwitchPrinting;
case object TrackSheep;
case object DnaSheep;

class LocalServer(universe: Universe[_ <: AnimalProcess]) extends Actor {
	def act() = {
		universe.start;
		
		var quit = false;
		while(!quit) {
			val cmd = scala.io.StdIn.readLine().split(' ');
			
			cmd(0) match {
				// TODO Importer un fichier d'adn
				
				case "position" => {
					if(cmd.length == 2) {
						val fileName = cmd(1);
						//val fileName = "pos_" + (System.currentTimeMillis / 1000).toString;
						
						val pw = new java.io.PrintWriter(new java.io.File(fileName));
						
						for(aP <- universe.map.animalProcesses.iterator if(aP.isAlive)) {
							val species = aP.animal match {
								case _: Sheep => "s";
								case _: Wolf => "w";
								case _ => "o";
							}
							
							val position = aP.animal.position._1 + "\t" + aP.animal.position._2;
							
							pw.write(species + "\t" + position + "\n");
						}
						
						pw.close();
					}
					else {
						Console.println("cmd usage: position fileName");
					}
				}
				
				case "stop" => {
					universe.stop = true;
					quit = true;
				}
				
				case "sl" => {
					universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Sheep => aP.isAlive; case _ => false;})) match {
						case Some(aP) => {
							aP.animal.tracker = Some(AnimalTrackerImpl);
						}
						
						case _ => Console.println("sorry there is no sheep alive");
					}
				}
				
				case "sa" => {
					universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Sheep => true; case _ => false;})) match {
						case Some(aP) => {
							Console.println(aP.animal.dna);
						}
						
						case _ => Console.println("sorry there is no sheep");
					}
				}
				
				case "wl" => {
					universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Wolf => aP.isAlive; case _ => false;})) match {
						case Some(aP) => {
							aP.animal.tracker = Some(AnimalTrackerImpl);
						}
						
						case _ => Console.println("sorry these is no wolf alive");
					}
				}
				
				case "wa" => {
					universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Wolf => true; case _ => false;})) match {
						case Some(aP) => {
							Console.println(aP.animal.dna);
						}
						
						case _ => Console.println("sorry these is no wolf");
					}
				}
				
				case "" => {
					universe.printing = !universe.printing;
				}
				
				case cmd => Console.println("unknown command: " + cmd);
			}
		}
		
		Console.println(this + " exits");
	}
}

trait Printer {
	def print(o: Any): Unit;
	def println(o: Any): Unit;
}

object ConsolePrinter extends Printer {
	def print(o: Any): Unit = Console.print(o);
	def println(o: Any): Unit = Console.println(o);
}

class PrintServer(server: DisplayServer) extends Printer {
	def print(o: Any): Unit = server ! Print(o)
	def println(o: Any): Unit = server ! Println(o);
}

case class Print(o: Any);
case class Println(o: Any);

class DisplayServer extends Actor {
	def act() = {
		loop {
			react {
				case Print(o) => Console.println(o);
				case Println(o) => Console.println(o);
				case _ => exit();
			}
		}
	}
}

//	class Server(port: Int, symbol: Symbol, universe: Universe[_ <: AnimalProcess]) extends Actor {
//		def act() = {
//			alive(port);
//			register(symbol, self);
//			
//			System.err.println("port: " + port);
//			System.err.println("symbol: " + symbol);
//			
//			universe.start;
//			
//			loop {
//				react {
//					case DnaSheep => {
//						universe.map.animals.lastOption match {
//							case Some(s: Sheep) => {
//								sender ! s.dna;
//							}
//							
//							case _ => sender ! true;
//						}
//					}
//					
//					case Stop => {
//						universe.stop = true;
//						
//						exit();
//					}
//					
//					case command => {
//						Console.println("unknown command: " + command);
//					}
//				}
//			}
//		}
//	}

trait Birth {
	def newBorn: Animal;
}

class BirthImp(val newBorn: Animal) extends Birth;

trait Death {
	def dead: Animal;
	def cause: CauseOfDeath;
}

class DeathImp(val dead: Animal, val cause: CauseOfDeath) extends Death;

trait TurnInfo {
	def time: Int;

	def births(species: AnimalSpecies): Iterable[Birth];
	def newBirth(birth: Birth): Unit;
	def nbBirths(species: AnimalSpecies): Int;
	def deaths(species: AnimalSpecies): Iterable[Death];
	def newDeath(death: Death): Unit;
	def nbDeaths(species: AnimalSpecies): Int;
	def actions: Iterable[Action];
	def newAction(action: Action): Unit;
	
	def nbSheep: Int;
	def nbWolves: Int;
}

class TurnInfoImp(
	val time: Int,
	val births: mutable.Map[AnimalSpecies, List[Birth]],
	val nbBirths: mutable.Map[AnimalSpecies, Int],
	val deaths: mutable.Map[AnimalSpecies, List[Death]],
	val nbDeaths: mutable.Map[AnimalSpecies, Int],
	val nbSheep: Int,
	val nbWolves: Int
) extends TurnInfo {
	def this(time: Int, nbSheep: Int, nbWolves: Int) = this(time, new mutable.HashMap[AnimalSpecies, List[Birth]](), new mutable.HashMap[AnimalSpecies, Int](), new mutable.HashMap[AnimalSpecies, List[Death]](), new mutable.HashMap[AnimalSpecies, Int](), nbSheep, nbWolves);
	
	def births(species: AnimalSpecies): Iterable[Birth] = births.getOrElse(species, Nil);
	def nbBirths(species: AnimalSpecies): Int = nbBirths.getOrElse(species, 0);
	def deaths(species: AnimalSpecies): Iterable[Death] = deaths.getOrElse(species, Nil);
	def nbDeaths(species: AnimalSpecies): Int = nbDeaths.getOrElse(species, 0);
	
	var actions: List[Action] = Nil;
	def newAction(action: Action): Unit = {
		actions = action :: actions;
	}
	
	def newBirth(birth: Birth): Unit = {
		births(Species(birth.newBorn)) = birth :: births.getOrElse(Species(birth.newBorn), Nil);
	}
	
	def newDeath(death: Death): Unit = {
		deaths(Species(death.dead)) = death :: deaths.getOrElse(Species(death.dead), Nil);
	}
}

trait TimeManager {
	def sleep(computationDuration: Long): Unit;
}

class FixedTurnDuration(val turnDuration: Int) extends TimeManager {
	def sleep(computationDuration: Long): Unit = {
		val sleepDuration = (turnDuration - computationDuration);
		
		if(sleepDuration > 0) {
			Thread.sleep(sleepDuration);
		}
	}
}

class ComputationRatio(val ratio: Double) extends TimeManager {
	def sleep(computationDuration: Long): Unit = {
		Thread.sleep((computationDuration * ratio).toLong);
	}
}

trait Logger {
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit; // Called at the end of each turn
	def stop: Unit; // Called at the end of the universe
}

class ModularLogger(val master: LoggerModule, val slaves: Iterable[LoggerModule]) extends Logger {
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		val trigger = master.master(time, universe);
		for(module <- slaves) module.slave(time, universe, trigger);
	}
	
	def stop: Unit = {
		master.stop;
		for(module <- slaves) module.stop;
	}
}

trait LoggerModule {
	def master(time: Int, universe: Universe[_ <: AnimalProcess]): Boolean;
	
	def slave(time: Int, universe: Universe[_ <: AnimalProcess], trigger: Boolean): Unit = {
		apply(time, universe);
		
		if(trigger) record(time);
	}
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit;
	def record(time: Int): Unit;
	
	def stop: Unit; // Called at the end of the universe
}

// This module logs regularly the different actions taken
class ActionsModule(fileName: String, val nbSteps: Int) extends LoggerModule {
	if((new java.io.File(fileName)).exists) {
		val cmd = scala.io.StdIn.readLine("Overwrite file " + fileName + "?");
		if(cmd != "y" && cmd != "yes") System.exit(0);
	}
	
	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	pw.write("# " + java.util.Calendar.getInstance().getTime() + "\n");
	pw.write("#time\taction\tnb\n");
	pw.flush;
	
	val data: mutable.Map[Action, Int] = mutable.Map[Action, Int]();
	
	var i = 0;
	
	def master(time: Int, universe: Universe[_ <: AnimalProcess]): Boolean = {
		apply(time, universe);
		
		if(i == nbSteps) {
			record(time);
			
			true;
		}
		else false;
	}
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		i += 1;
		
		{var l = universe.currentTurnInfo.actions;
		while(l.nonEmpty) {
			val action = l.head;
			data(action) = (data.getOrElse(action, 0) + 1);
			
			l = l.tail;
		}}
	}
	
	def record(time: Int): Unit = {
		for((action, count) <- data if count > 0) {
			pw.write(time + "\t" + action + "\t" + Maths.average(count, i) + "\n");
			
			data(action) = 0; // Reset
		}
		
		pw.flush;
		
		i = 0;
	}
	
	def stop: Unit = {
		pw.close;
	}
}

// This module logs regularly the causes of death
class DeathModule(fileName: String, val nbSteps: Int) extends LoggerModule {
	if((new java.io.File(fileName)).exists) {
		val cmd = scala.io.StdIn.readLine("Overwrite file " + fileName + "?");
		if(cmd != "y" && cmd != "yes") System.exit(0);
	}
	
	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	pw.write("# " + java.util.Calendar.getInstance().getTime() + "\n");
	pw.write("#time\tspecies\tcauseOfDeath\tcount\n");
	pw.flush;
	
	val dataW: mutable.Map[CauseOfDeath, Int] = mutable.Map[CauseOfDeath, Int]();
	val dataS: mutable.Map[CauseOfDeath, Int] = mutable.Map[CauseOfDeath, Int]();
	
	var i = 0;
	
	def master(time: Int, universe: Universe[_ <: AnimalProcess]): Boolean = {
		apply(time, universe);
		
		if(i == nbSteps) {
			record(time);
			
			true;
		}
		else false;
	}
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		i += 1;
		
		val turnInfo = universe.currentTurnInfo;
		
		for(death <- turnInfo.deaths(WolfType)) dataW(death.cause) = (dataW.getOrElse(death.cause, 0) + 1);
		for(death <- turnInfo.deaths(SheepType)) dataS(death.cause) = (dataS.getOrElse(death.cause, 0) + 1);
	}
	
	def record(time: Int): Unit = {
		for((cause, count) <- dataW if count > 0) {
			pw.write(time + "\twolf\t" + cause + "\t" + Maths.average(count, i) + "\n");
			
			dataW(cause) = 0; // Reset
		}
		
		for((cause, count) <- dataS if count > 0) {
			pw.write(time + "\tsheep\t" + cause + "\t" + Maths.average(count, i) + "\n");
			
			dataS(cause) = 0; // Reset
		}
		
		pw.flush;
		
		i = 0;
	}
	
	def stop: Unit = {
		pw.close;
	}
}

// This module logs regularly information about the behaviour of the animals
class BehaviourModule(fileName: String, val nbSteps: Int) extends LoggerModule {
	if((new java.io.File(fileName)).exists) {
		val cmd = scala.io.StdIn.readLine("Overwrite file " + fileName + "?");
		if(cmd != "y" && cmd != "yes") System.exit(0);
	}
	
	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	pw.write("# " + java.util.Calendar.getInstance().getTime() + "\n");
	pw.write("#time\twolfImpact\n");
	pw.flush;
	
	var sum: Double = 0.0;
	var nb: Int = 0;
	
	var i = 0;
	
	def master(time: Int, universe: Universe[_ <: AnimalProcess]): Boolean = {
		apply(time, universe);
		
		if(i == nbSteps) {
			record(time);
			
			true;
		}
		else false;
	}
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		i += 1;
		
		// For the first sheep found (if any), computes the Jensen-Shannon divergence of the actions distributions with and without the wolves on sight (if there is none, the jsd = 0)
		@tailrec
		def loop(it: Iterator[AnimalProcess], c: Int): Unit = if(c > 0 && it.hasNext) it.next.animal match {
			case sheep: Sheep => {
				val perceptionVec = sheep.perception(0, universe.map);
				
				val vec1 = sheep.dna.interact(perceptionVec);
				
				// Wolves are removed from sight
				{var j = 0;
				var k = 1; // Wolves appear at position 1 [infoPerTile]
				while(j < (2 * sheep.reference.sightRange + 1) * (2 * sheep.reference.sightRange + 1)) {
					perceptionVec(k) = 0;
					
					k += sheep.reference.infoPerTile;
					
					j += 1;
				}}
				
				val vec2 = sheep.dna.interact(perceptionVec);
				
				val jsd = Maths.JSD(vec1, vec2);
				
				sum += jsd;
				nb += 1;
				
				loop(it, (c - 1));
			}
			
			case _ => loop(it, c);
		}
		
		loop(universe.map.animalProcesses.iterator, 10);
	}
	
	def record(time: Int): Unit = {
		val avg = Maths.average(sum, nb);
		pw.write(time + "\t" + "%.5f".format(avg) + "\n");
		pw.flush;
		
		// reset
		sum = 0.0;
		nb = 0;
		
		i = 0;
	}
	
	def stop: Unit = {
		pw.close;
	}
}

// This module logs regularly the size of the populations as well as their respective life expectancy
class DemographicModule(fileName: String, val nbSteps: Int) extends LoggerModule {
	if((new java.io.File(fileName)).exists) {
		val cmd = scala.io.StdIn.readLine("Overwrite file " + fileName + "?");
		if(cmd != "y" && cmd != "yes") System.exit(0);
	}
	
	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	pw.write("# " + java.util.Calendar.getInstance().getTime() + "\n");
	pw.write("#time\tnbSheep\tnbWolves\tsheepLifeExpectancy\twolfLifeExpectancy\tavgSheepAge\tavgWolvesAge\tavgSheepFood\tavgWolvesFood\tstatsSheep\tstatsWolves\tavgSheepChildren\tavgWolvesChildren\tsheepFertilityRate\twolvesFertilityRate\n");
	pw.flush;
	
	// TODO HERE Calculer aussi les variances
	
	// Life expectancy and fertility rate
	var dataWolfDeath: (Int, Int, Int) = (0, 0, 0); // (sum of age, sum of children, number of individuals)
	var dataSheepDeath: (Int, Int, Int) = (0, 0, 0); // (sum of age, sum of children, number of individuals)
	
	var nbWolvesAlive: Int = 0;
	var nbSheepAlive: Int = 0;
	
	var sumWolvesAge: Int = 0;
	var sumSheepAge: Int = 0;
	
	var sumWolvesFood: Double = 0.0;
	var sumSheepFood: Double = 0.0;
	
	var sumWolvesChildren: Int = 0;
	var sumSheepChildren: Int = 0;
	
	// Average (in abs), number of params, norm2
	var dataStatsWolves: (Double, Long, Double) = (0.0, 0, 0.0);
	var dataStatsSheep: (Double, Long, Double) = (0.0, 0, 0.0); // (sum of avg of abs, sum of nbr of params, sum of norm2)
	
	var i = 0;
	var lastNbWolves = 0.0;
	var lastNbSheep = 0.0;
	
	def master(time: Int, universe: Universe[_ <: AnimalProcess]): Boolean = {
		apply(time, universe);
		
		val demographics = universe.map.demographics;
		if(i == nbSteps ||  Maths.variation(demographics._1, lastNbWolves, 0.2) ||  Maths.variation(demographics._2, lastNbSheep, 0.2)) {
		//if(i == nbSteps ||  Maths.variation(Maths.average(nbWolvesAlive, i), lastNbWolves, 0.1) ||  Maths.variation(Maths.average(nbSheepAlive, i), lastNbSheep, 0.1)) {
			record(time);
			
			true;
		}
		else false;
	}
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		i += 1;
		
		val turnInfo = universe.currentTurnInfo;
		
		dataWolfDeath = turnInfo.deaths(WolfType).foldLeft(dataWolfDeath)((acc: (Int, Int, Int), x: Death) => ((acc._1 + x.dead.age), (acc._2 + x.dead.nbChildren), (acc._3 + 1)));
		dataSheepDeath = turnInfo.deaths(SheepType).foldLeft(dataSheepDeath)((acc: (Int, Int, Int), x: Death) => ((acc._1 + x.dead.age), (acc._2 + x.dead.nbChildren), (acc._3 + 1)));
		
		val demographics = universe.map.demographics; // Values at the end of the turn;
		nbWolvesAlive += demographics._1;
		nbSheepAlive += demographics._2;
		
		for(animalProcess <- universe.map.animalProcesses) animalProcess.animal match {
			case wolf: Wolf => {
				val stats = wolf.dna.stats; // sum and max (in abs) + number of params
				val norm2 = wolf.dna.norm2;
				
				sumWolvesAge += wolf.age;
				sumWolvesFood += wolf.food;
				sumWolvesChildren += wolf.nbChildren;
				
				dataStatsWolves = ((dataStatsWolves._1 + (stats._1 / stats._3.toDouble)), (dataStatsWolves._2 + stats._3), (dataStatsWolves._3 + norm2));
			}
			
			case sheep: Sheep => {
				val stats = sheep.dna.stats; // sum and max (in abs) + number of params
				val norm2 = sheep.dna.norm2;
				
				sumSheepAge += sheep.age;
				sumSheepFood += sheep.food;
				sumSheepChildren += sheep.nbChildren;
				
				dataStatsSheep = ((dataStatsSheep._1 + (stats._1 / stats._3.toDouble)), (dataStatsSheep._2 + stats._3), (dataStatsSheep._3 + norm2));
			}
		}
	}
	
	def record(time: Int): Unit = {
		val nbSheep = Maths.average(nbSheepAlive, i);
		val nbWolves = Maths.average(nbWolvesAlive, i);
		
		val sheepLifeExpectancy = Maths.average(dataSheepDeath._1, dataSheepDeath._3);
		val wolfLifeExpectancy = Maths.average(dataWolfDeath._1, dataWolfDeath._3);
		
		val avgSheepAge: Double = Maths.average(sumSheepAge, nbSheepAlive);
		val avgWolvesAge: Double = Maths.average(sumWolvesAge, nbWolvesAlive);
		
		val avgSheepFood: Double = Maths.average(sumSheepFood, nbSheepAlive);
		val avgWolvesFood: Double = Maths.average(sumWolvesFood, nbWolvesAlive);
		
		val statsSheep = List("%.3f".format(Maths.average(dataStatsSheep._1, nbSheepAlive)), "%.3f".format(Maths.average(dataStatsSheep._2, nbSheepAlive)), "%.3f".format(Maths.average(dataStatsSheep._3, nbSheepAlive)));
		val statsWolves = List("%.3f".format(Maths.average(dataStatsWolves._1, nbWolvesAlive)), "%.3f".format(Maths.average(dataStatsWolves._2, nbWolvesAlive)), "%.3f".format(Maths.average(dataStatsWolves._3, nbWolvesAlive)));
		
		val avgSheepChildren: Double = Maths.average(sumSheepChildren, nbSheepAlive);
		val avgWolvesChildren: Double = Maths.average(sumWolvesChildren, nbWolvesAlive);
		
		val sheepFertilityRate = Maths.average(dataSheepDeath._2, dataSheepDeath._3);
		val wolfFertilityRate = Maths.average(dataWolfDeath._2, dataWolfDeath._3);
		
		pw.write(time + "\t" + "%.3f".format(nbSheep) + "\t" + "%.3f".format(nbWolves) + "\t" + "%.3f".format(sheepLifeExpectancy) + "\t" + "%.3f".format(wolfLifeExpectancy) + "\t" + "%.3f".format(avgSheepAge) + "\t" + "%.3f".format(avgWolvesAge) + "\t" + "%.3f".format(avgSheepFood) + "\t" + "%.3f".format(avgWolvesFood) + "\t" + statsSheep.mkString("\t") + "\t" + statsWolves.mkString("\t") + "\t" + "%.3f".format(avgSheepChildren) + "\t" + "%.3f".format(avgWolvesChildren) + "\t" + "%.6f".format(sheepFertilityRate) + "\t" + "%.6f".format(wolfFertilityRate) + "\n");
		
		pw.flush;
		
		// 
		lastNbWolves = nbWolves;
		lastNbSheep = nbSheep;
		
		// Reset
		dataWolfDeath = (0, 0, 0);
		dataSheepDeath = (0, 0, 0);
		
		nbWolvesAlive = 0;
		nbSheepAlive = 0;
		
		sumWolvesAge = 0;
		sumSheepAge = 0;

		sumWolvesFood = 0.0;
		sumSheepFood = 0.0;
		
		sumWolvesChildren = 0;
		sumSheepChildren = 0;

		dataStatsWolves = (0.0, 0, 0.0);
		dataStatsSheep = (0.0, 0, 0.0);
		
		i = 0;
	}
	
	def stop: Unit = {
		pw.close;
	}
}

//// This module logs logarithmically the positions of each animals
//class PositionModule(val logPath: String, val exponent: Int = 10) extends LoggerModule {
//	var i = 0;
//	var k = 1;
//	
//	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
//		if(time % k == 0) {
//			i += 1;
//			
//			if(i == exponent) {
//				i = 1;
//				k *= exponent;
//			}
//			
//			(new java.io.File(logPath + "/" + time.toString)).mkdir;
//			val fileName = logPath + "/" + time.toString + "/" + "positions.txt";
//			
//			val pw = new java.io.PrintWriter(new java.io.File(fileName));
//			pw.write("# " + java.util.Calendar.getInstance().getTime() + "\n");
//			
//			for(aP <- universe.map.animalProcesses.iterator) {
//				val species = aP.animal match {
//					case _: Sheep => "s";
//					case _: Wolf => "w";
//					case _ => "o";
//				}
//				
//				val position = aP.animal.position._1 + "\t" + aP.animal.position._2;
//				
//				pw.write(species + "\t" + position + "\n");
//			}
//			
//			pw.close();
//		}
//	}
//}

trait Universe[T <: AnimalProcess] extends Actor {
	var currentTurnInfo: TurnInfo = null;
	val runtime = Runtime.getRuntime;
	
	def map: Map[T];
	var stop: Boolean;
	val nbIter: Int; // -1 ~ never
	var printing: Boolean;
	def timeManagerOpt: Option[TimeManager];
	
	def birth(animalProcess: T): Unit = {
		currentTurnInfo.newBirth(new BirthImp(animalProcess.animal));
	}
	
	def death(animalProcess: T, cause: CauseOfDeath): Unit = {
		currentTurnInfo.newDeath(new DeathImp(animalProcess.animal, cause));
	}
	
	def actionLoop(time: Int): Unit;
	
	def onStop(): Unit = {
		currentTurnInfo = null;
		map.clear;
		System.gc;
		
		Console.println(java.util.Calendar.getInstance().getTime() + " - " + this + " exits");
	}
	
	def act(): Unit = {
		Console.println(java.util.Calendar.getInstance().getTime() + " - " + this + " starts");
		
		var phase = 0;

		val infoTimer = 1000; // Every 1000 ms
		var timer: Long = 0;
		var printInfo = true;
		
		// TODO these two should be parameters
		val maxGenerateWolf = 200;
		val maxGenerateSheep = 200;

		var time = 0;
	
		while(!stop && (nbIter != time)) {
			val (nextPhase, nextTimer, nextPrintInfo) = executeTurn(time, phase, timer, printInfo, infoTimer, maxGenerateWolf, maxGenerateSheep);
			
			phase = nextPhase;
			timer = nextTimer;
			printInfo = nextPrintInfo;
			
			time += 1;
		}
		
		onStop();
	}
	
	def executeTurn(time: Int, phase: Int, timer: Long, printInfo: Boolean, infoTimer: Int, maxGenerateWolf: Int, maxGenerateSheep: Int): (Int, Long, Boolean) = {
		val timestamp0 = System.currentTimeMillis;
			   
		val (nbWolves, nbSheep) = map.demographics;
		currentTurnInfo = new TurnInfoImp(time, nbSheep, nbWolves); // So these numbers correspond to the beginning of the turn (and not when the logging is done)
			
		if(printing && printInfo) {
			System.err.print("Turn " + time + " - ");
			System.err.print("wolves: " + nbWolves + "; sheep: " + nbSheep + " - ");
		}
		
		// Demographic control
		if(phase == 0) {
			if(nbSheep < maxGenerateSheep) map.generateSheep(this);
			if(nbWolves < maxGenerateWolf) map.generateWolf(this);
		}
		
		// Animals' decision taking and acting
		actionLoop(time);
		
		// Various minor computations
		val nextPhase = if((phase == 0) && (nbWolves > 2 * maxGenerateWolf) && (nbSheep > 2 * maxGenerateSheep)) {
			System.err.println("now phase 1 (" + time + ")");
			
			1;
		}
		else {
			if((phase == 1) && (nbWolves == 0 || nbSheep == 0)) {
				stop = true;
				
				Console.println(time + " - wolves: " + nbWolves + "; sheep: " + nbSheep + "!!");
			}
			
			phase;
		}
		
		if(printing && printInfo) {
			val sheepLifeExpectancy = currentTurnInfo.deaths(SheepType).foldLeft((0, 0))((acc: (Int, Int), x: Death) => (acc._1 + x.dead.age, acc._2 + 1)) match {
				case (_, 0) => 0;
				case (sum, count) => sum / count.toDouble; 
			}
			
			val wolfLifeExpectancy = currentTurnInfo.deaths(WolfType).foldLeft((0, 0))((acc: (Int, Int), x: Death) => (acc._1 + x.dead.age, acc._2 + 1)) match {
				case (_, 0) => 0;
				case (sum, count) => sum / count.toDouble; 
			}
			
			System.err.print("life exp: " + "%.1f".format(wolfLifeExpectancy) + "; " + "%.1f".format(sheepLifeExpectancy) + " - ");
			System.err.print((runtime.totalMemory - runtime.freeMemory) / (1024 * 1024) + " Mo - ");
		}
		
		val timestamp1 = System.currentTimeMillis; // Time after main computation
		
		val computationDuration = (timestamp1 - timestamp0);
		
		if(printing && printInfo) {
			System.err.print(computationDuration + " ms\n");
		}
		
		timeManagerOpt match {
			case Some(timeManager: TimeManager) => timeManager.sleep(computationDuration);
			case None => ();
		}
		
		val (nextTimer, nextPrintInfo) = if(timestamp1 - timer > infoTimer) {
			(timestamp1, true);
		}
		else (timer, false);
		
		(nextPhase, nextTimer, nextPrintInfo);
	}
}

case class ChooseAction(val time: Int, val map: Map[AnimalActor]);
case class Choice(val animalActor: AnimalActor, val action: Action);

class UniversePar(
	val map: Map[AnimalActor],
	var stop: Boolean,
	val nbIter: Int,
	var printing: Boolean,
	val nbProc: Int,
	val timeManagerOpt: Option[TimeManager]
) extends Universe[AnimalActor] {
	override def birth(animalActor: AnimalActor): Unit = {
		super.birth(animalActor);
		
		animalActor.start;
	}
	
	override def death(animalActor: AnimalActor, cause: CauseOfDeath): Unit = {
		super.death(animalActor, cause);
		
		animalActor ! Stop;
	}

	def actionLoop(time: Int): Unit = {
		val animalActors = map.animalProcesses.iterator;
		
		var i = 0;
		
		while(i < nbProc && animalActors.hasNext) {
			animalActors.next ! ChooseAction(time, map);
			
			i += 1;
		}
		
		while(animalActors.hasNext) {
			receive {
				case Choice(animalActor, action) => {
					(animalActors find (_.isAlive)) match {
						case Some(aP) => aP ! ChooseAction(time, map);
						case None => ();
					}
					
					if(animalActor.isAlive) { // It's unlikely but the animal might have died
						animalActor.performAction(action, time, map, this);
						animalActor.getOlder(map, this);
						
						currentTurnInfo.newAction(action);
					}
				}
				
				//case _ => ();
			}
		}
	   
		for(j <- 1 to i) {
			receive {
				case Choice(animalActor, action) => {
					if(animalActor.isAlive) { // It's unlikely but the animal might have died
						animalActor.performAction(action, time, map, this);
						animalActor.getOlder(map, this);
						
						currentTurnInfo.newAction(action);
					}
				}
				
				//case _ => ();
			}
		}
	}
	
	override def onStop(): Unit = {
		for(animalActor <- map.animalProcesses) {
			animalActor ! Stop;
		}
		
		super.onStop();
	}
}

class UniverseParLogger(
	map: Map[AnimalActor],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	nbProc: Int,
	timeManagerOpt: Option[TimeManager],
	val loggerOpt: Option[Logger]
) extends UniversePar(map, stop0, nbIter, printing0, nbProc, timeManagerOpt) {
	override def executeTurn(time: Int, phase: Int, timer: Long, printInfo: Boolean, infoTimer: Int, maxGenerateWolf: Int, maxGenerateSheep: Int): (Int, Long, Boolean) = {
		val returnValue = super.executeTurn(time, phase, timer, printInfo, infoTimer, maxGenerateWolf, maxGenerateSheep);
		
		loggerOpt match {
			case Some(logger) => logger(time, this);
			case _ => ();
		}
		
		returnValue;
	}
	
	override def onStop(): Unit = {
		loggerOpt match {
			case Some(logger) => logger.stop;
			case _ => ();
		}
		
		super.onStop();
	}
}

class UniverseParBenchmark(
	map: Map[AnimalActor],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	nbProc: Int,
	val warmUp: Int
) extends UniversePar(map, stop0, nbIter, printing0, nbProc, None) {
	override def birth(animalActor: AnimalActor): Unit = {
		animalActor.start;
	}
	
	override def death(animalActor: AnimalActor, cause: CauseOfDeath): Unit = {
		animalActor ! Stop;
	}
	
	override def actionLoop(time: Int): Unit = {
		val animalActors = map.animalProcesses.iterator;
		
		var i = 0;
		
		while(i < nbProc && animalActors.hasNext) {
			animalActors.next ! ChooseAction(time, map);
			
			i += 1;
		}
		
		while(animalActors.hasNext) {
			receive {
				case Choice(animalActor, action) => {
					(animalActors find (_.isAlive)) match {
						case Some(aP) => aP ! ChooseAction(time, map);
						case None => ();
					}
					
					// Actions are not executed
				}
				
				//case _ => ();
			}
		}
		
		for(j <- 1 to i) {
			receive {
				case Choice(animalActor, action) => {
					// Actions are not executed
				}
				
				//case _ => ();
			}
		}
	}
	
	override def act(): Unit = {
		Console.print("warm up phase");
		
		for(time <- 0 until warmUp) {
			actionLoop(time);
		}
		
		Console.println(" - complete");
		
		val beginTime = System.currentTimeMillis;
		
		for(time <- 0 until nbIter) {
			//Console.println("iter " + time);
			actionLoop(time);
		}
		
		val endTime = System.currentTimeMillis;
		
		Console.println((endTime - beginTime) + " ms");
		
		onStop();
	}
}

class UniverseSeq(
	val map: Map[AnimalSimple],
	var stop: Boolean,
	val nbIter: Int,
	var printing: Boolean,
	val timeManagerOpt: Option[TimeManager]
) extends Universe[AnimalSimple] {
	def actionLoop(time: Int): Unit = {
		for(animalSimple <- map.animalProcesses if animalSimple.isAlive) {
			// animalSimple.act(time, map, this);
			val action = animalSimple.animal.chooseAction(time, map);
			animalSimple.performAction(action, time, map, this);
			
			animalSimple.getOlder(map, this);
			
			currentTurnInfo.newAction(action);
		}
	}
}

class UniverseSeqLogger(
	map: Map[AnimalSimple],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	timeManagerOpt: Option[TimeManager],
	val loggerOpt: Option[Logger]
) extends UniverseSeq(map, stop0, nbIter, printing0, timeManagerOpt) {
	override def executeTurn(time: Int, phase: Int, timer: Long, printInfo: Boolean, infoTimer: Int, maxGenerateWolf: Int, maxGenerateSheep: Int): (Int, Long, Boolean) = {
		val returnValue = super.executeTurn(time, phase, timer, printInfo, infoTimer, maxGenerateWolf, maxGenerateSheep);
		
		loggerOpt match {
			case Some(logger) => logger(time, this);
			case _ => ();
		}
		
		returnValue;
	}
	
	override def onStop(): Unit = {
		loggerOpt match {
			case Some(logger) => logger.stop;
			case _ => ();
		}
		
		super.onStop();
	}
}

class UniverseSeqBenchmark(
	map: Map[AnimalSimple],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	val warmUp: Int
) extends UniverseSeq(map, stop0, nbIter, printing0, None) {
	override def birth(animalSimple: AnimalSimple): Unit = ();
	
	override def death(animalSimple: AnimalSimple, cause: CauseOfDeath): Unit = ();
	
	override def actionLoop(time: Int): Unit = {
		for(animalSimple <- map.animalProcesses if animalSimple.isAlive) {
			animalSimple.animal.chooseAction(time, map);
		}
	}
	
	override def act(): Unit = {
		Console.print("warm up phase");
		
		for(time <- 0 until warmUp) {
			actionLoop(time);
		}
		
		Console.println(" - complete");
		
		val beginTime = System.currentTimeMillis;
		
		for(time <- 0 until nbIter) {
			//Console.println("iter " + time);
			actionLoop(time);
		}
		
		val endTime = System.currentTimeMillis;
		
		Console.println((endTime - beginTime) + " ms");
		
		onStop();
	}
}

object Grass {
	val maxHeight = 20;
	val growthRate = 1;
}

class Grass(var height: Int, val maxHeight: Int, var time: Int) {
	def update(newTime: Int): Int = {
		if(newTime > time) {
			height = min(maxHeight, (height + Grass.growthRate * (newTime - time)));
			time = newTime;
		}
		
		height;
	}
	
	def cut(quantity: Int): Unit = {
		height -= quantity;
	}
}

class Tile[T <: AnimalProcess](var animalProcess: Option[T], val grass: Grass);

trait Map[T <: AnimalProcess] {
	def length: Int;
	def height: Int;
	def animalProcesses: mutable.Set[T];
	
	def processFactory: (Animal => T);
	
	def tile(pos: (Int, Int)): Option[Tile[T]];
	def addPosDir(a: (Int, Int), b: (Int, Int)): (Int, Int);
	def neighbourhood(pos: (Int, Int), dst: Int): List[((Int, Int), Tile[T])];
	def print: Unit;
	
	def clear: Unit; // Function called to free memory at the end of the universe
	
	// (nbWolves, nbSheep)
	def demographics: (Int, Int) = {
		@tailrec
		def aux(iter: Iterator[T], w: Int, s: Int): (Int, Int) = iter.hasNext match {
			case false => (w, s);
			case true => iter.next.animal match {
				case _: Wolf => aux(iter, (w + 1), s);
				case _: Sheep => aux(iter, w, (s + 1));
			}
		}
		
		aux(animalProcesses.iterator, 0, 0);
	}
	
	def birth(animal: Animal, universe: Universe[T]): Unit = {
		val animalProcess = processFactory(animal);
		
		tile(animal.position).get.animalProcess = Some(animalProcess);
		animalProcesses += animalProcess;
		
		universe.birth(animalProcess);
	}
	
	def death(animalProcess: T, cause: CauseOfDeath, universe: Universe[T]): Unit = {
		tile(animalProcess.animal.position).get.animalProcess = None;
		animalProcesses -= animalProcess;
		
		universe.death(animalProcess, cause);
	}
	
	def generateWolf(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean;
	
	def generateSheep(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean;
}

class MapImp[T <: AnimalProcess](
	val length: Int,
	val height: Int,
	val tiles: Array[Array[Tile[T]]],
	val animalProcesses: mutable.Set[T],
	val processFactory: (Animal => T),
	val wolfFactory: (((Int, Int)) => Wolf),
	val sheepFactory: (((Int, Int)) => Sheep),
	var nbWolves: Int,
	var nbSheep: Int,
	var goodWolf: Option[(Int, Wolf)] = None,
	var goodSheep: Option[(Int, Sheep)] = None
) extends Map[T] {
	// (nbWolves, nbSheep)
	override def demographics: (Int, Int) = (nbWolves, nbSheep);
	
	override def birth(animal: Animal, universe: Universe[T]): Unit = {
		super.birth(animal, universe);
		
		animal match {
			case _: Wolf => nbWolves += 1;
			case _: Sheep => nbSheep += 1;
		}
	}
	
	override def death(animalProcess: T, cause: CauseOfDeath, universe: Universe[T]): Unit = {
		super.death(animalProcess, cause, universe);
		
		animalProcess.animal match {
			case w: Wolf => {
				nbWolves -= 1;
				
				//val score = (ceil(100 * (1 - exp(- w.ageLineage / 200.0))) * w.age).toInt;
				val score = ((1 - exp(- 1 - w.generation)) * (if(w.generation > 0) 2 else 1) * w.age * (if(cause == Starvation) 1 else 2)).toInt;
				
				if(goodWolf.isEmpty || score > goodWolf.get._1) {
					goodWolf = Some((score, w));
					//System.err.println("best wolf score: " + score + " (" + w.age + ", " + w.ageLineage + ")");
				}
			}
			case s: Sheep => {
				nbSheep -= 1;
				
				//val score = (ceil(100 * (1 - exp(- s.ageLineage / 100.0))) * s.age).toInt;
				//val score = s.age * s.generation;
				val score = ((1 - exp(- 1 - s.generation)) * (if(s.generation > 0) 2 else 1) * s.age * (if(cause == Starvation) 1 else 2)).toInt;
				
				if(goodSheep.isEmpty || score > goodSheep.get._1) {
					goodSheep = Some((score, s));
					//System.err.println("best sheep score: " + score + " (" + s.age + ", " + s.ageLineage + ")");
				}
				else if(scala.util.Random.nextDouble > (1 - exp(- 100 * (goodSheep.get._1 - score) / goodSheep.get._1.toDouble))) goodSheep = Some((score, s));
			}
		}
	}
	
	override def generateWolf(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean = {
		val position = (randGen.nextInt(length), randGen.nextInt(height));
		
		if(tile(position).get.animalProcess.isEmpty) {
			val wolf = if(!goodWolf.isEmpty && randGen.nextDouble > exp(- goodWolf.get._1 / 1600.0)) {
				val tmpWolf = goodWolf.get._2.generateChild(scala.util.Random, true);
				tmpWolf.position = position;
				
				tmpWolf;
			}
			else {
				wolfFactory(position);
			}

			birth(wolf, universe);
			
			true;
		}
		else false;
	}
	
	override def generateSheep(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean = {
		val position = (randGen.nextInt(length), randGen.nextInt(height));
		
		if(tile(position).get.animalProcess.isEmpty) {
			val sheep = if(!goodSheep.isEmpty && randGen.nextDouble > exp(- goodSheep.get._1 / 1600.0)) {
				val tmpSheep = goodSheep.get._2.generateChild(scala.util.Random, true);
				tmpSheep.position = position;
				
				tmpSheep;					
			}
			else {
				sheepFactory(position);
			}
			
			birth(sheep, universe);
			
			true;
		}
		else false;
	}
	
	// Return coordinates between 0 and max
	def convertPos(pos: (Int, Int)): (Int, Int) = pos match {
		case (x, y) => ((((x + length) % length) + length) % length, (((y + height) % height) + height) % height);
	}
	
	// Return coordinates between 0 and max
	def convertPos(x: Int, y: Int): (Int, Int) = convertPos((x, y));
	
	def tile(pos: (Int, Int)): Option[Tile[T]] = {
		// Torus map
		convertPos(pos) match {
			case (x, y) => Some(tiles(x)(y));
		}
		
		// Rectangular map
//			if(x >= 0 && y >= 0 && x < length && y < height) Some(tiles(x)(y));
//			else None;
	}
	
	def tile(x: Int, y: Int): Option[Tile[T]] = tile((x, y));
	
	def addPosDir(a: (Int, Int), b: (Int, Int)): (Int, Int) = convertPos((a._1 + b._1, a._2 + b._2));
	
	def neighbourhood(pos: (Int, Int), dst: Int): List[((Int, Int), Tile[T])] = {
		// Torus map
		def aux(l: List[Int]): ((Int, Int), Tile[T]) = {
			val pos = convertPos((l.head, l.tail.head));
			
			(pos, tiles(pos._1)(pos._2));
		}
		
		Maths.product(List(List.range(pos._1 - dst, pos._1 + dst), List.range(pos._2 - dst, pos._2 + dst))).map(aux);
		
		// Rectangular map
//			@tailrec
//			def aux(l: List[(Int, Int)], result: List[((Int, Int), Tile[T])]): List[((Int, Int), Tile[T])] = l match {
//				case Nil => result;
//				
//				case h::t => tile(h) match {
//					case None => aux(t, result);
//					case Some(tile) => aux(t, ((h, tile) :: result));
//				}
//			}
//			
//			aux(Maths.product(List(List.range(pos._1 - dst, pos._1 + dst), List.range(pos._2 - dst, pos._2 + dst))).map(l => (l.head, l.tail.head)), Nil);
	}
	
	def print = {
		for(x <- 0 to (length - 1)) {
			for(y <- 0 to (height - 1)) {
				val symbol = tiles(x)(y).animalProcess match {
					case None => '_';
					case Some(aP) => aP.animal match {
						case _: Wolf => 'w';
						case _: Sheep => 's';
						case _ => 'u';
					}
				}
				
				Console.print(symbol + " ");
			}
			
			Console.println("");
		}
	}
	
	// Function called to free memory at the end of the universe
	def clear: Unit = {
		animalProcesses.clear;
		
		{val emptyArray: Array[Tile[T]] = Array[Tile[T]]();
		var i = 0;
		while(i < tiles.length) {
			tiles(i) = emptyArray;
			
			i += 1;
		}}
		
		goodWolf = None;
		goodSheep = None;
	}
}

object Benchmark {
	def main(args: Array[String]): Unit = {
		val argumentsNames: immutable.Set[String] = immutable.Set[String](
			"configFile",
			"benchType",
			"nbSheeps",
			"sheepDna",
			"nbWolves",
			"wolfDna",
			"nbProc",
			"nbIter",
			"warmUp"
		);
		
		val arguments = Main.parseArguments(argumentsNames, args);
		
		// Read the configuration file if one is given as argument
		arguments.get("configFile") match {
			case Some(fileName) => {
				arguments ++= Source.fromFile(fileName).getLines.map(_.split("\t", 2)).map(x => (x(0), x(1)));
			}
			
			case None => ();
		}
		
		val benchType: String = Try(arguments("benchType")).toOption match {
			case None => "universePar";
			case Some(value) => value;
		}
		
		val nbSheep: Int = Try(arguments("nbSheep").toInt).toOption match {
			case None => 0;
			case Some(value) => value;
		}
		
		val nbWolves: Int = Try(arguments("nbWolves").toInt).toOption match {
			case None => 0;
			case Some(value) => value;
		}
		
		val sheepFactory: (((Int, Int)) => Sheep) = Main.loadSheepFactory(arguments);
		
		val wolfFactory: (((Int, Int)) => Wolf) = Main.loadWolfFactory(arguments);
		
		val nbProc: Int = Try(arguments("nbProc").toInt).toOption match {
			case None => 4;
			case Some(value) => value;
		}
		
		val nbIter: Int = Try(arguments("nbIter").toInt).toOption match {
			case None => 10;
			case Some(value) => value;
		}
		
		val warmUp: Int = Try(arguments("warmUp").toInt).toOption match {
			case None => 10;
			case Some(value) => value;
		}
		
		val mapLength, mapHeight = (nbSheep + nbWolves + 1);
		
		val randGen: scala.util.Random = scala.util.Random;
		
		benchType match {
			case "universePar" => {
				def processFactory(animal: Animal): AnimalActor = {
					new AnimalActorImp(animal);
				}
				
				// Generate the map
				val map = new MapImp[AnimalActor](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile(None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalActor](), processFactory, wolfFactory, sheepFactory, 0, 0);
				
				val universe = new UniverseParBenchmark(map, false, nbIter, false, nbProc, warmUp);
				
				for(i <- 0 until nbSheep) {
					val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
					
					val sheep = sheepFactory(position);
					map.birth(sheep, universe);
				}
				
				for(i <- 0 until nbWolves) {
					val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
					
					val wolf = wolfFactory(position);
					map.birth(wolf, universe);
				}
				
				universe.start;
			}
			
			case "universeSeq" => {
				def processFactory(animal: Animal): AnimalSimple = {
					new AnimalSimpleImp(animal);
				}
				
				// Generate the map
				val map = new MapImp[AnimalSimple](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile(None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalSimple](), processFactory, wolfFactory, sheepFactory, 0, 0);
				
				val universe = new UniverseSeqBenchmark(map, false, nbIter, false, warmUp);
				
				for(i <- 0 until nbSheep) {
					val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
					
					val sheep = sheepFactory(position);
					map.birth(sheep, universe);
				}
				
				for(i <- 0 until nbWolves) {
					val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
					
					val wolf = wolfFactory(position);
					map.birth(wolf, universe);
				}
				
				universe.start;
			}
			
			case _ => Console.println("Unknown benchmark type \"" + benchType + "\".");
		}
	}
}

object MemoryConsumption {
	def main(args: Array[String]): Unit = {
		val randGen: scala.util.Random = scala.util.Random;
		
		val argumentsNames: immutable.Set[String] = immutable.Set[String](
			"configFile",
			"mapLength",
			"mapHeight",
			"sheepDna",
			"wolfDna",
			"nbSheeps",
			"nbWolves"
		);
		
		val arguments: mutable.Map[String, String] = Main.parseArguments(argumentsNames, args);
		
		// Read the configuration file if one is given as argument
		arguments.get("configFile") match {
			case Some(fileName) => {
				arguments ++= Source.fromFile(fileName).getLines.map(_.split("\t", 2)).map(x => (x(0), x(1)));
			}
			
			case None => ();
		}
		
		val mapLength: Int = Try(arguments("mapLength").toInt).toOption match {
			case None => 128;
			case Some(value) => value;
		}
		
		val mapHeight: Int = Try(arguments("mapHeight").toInt).toOption match {
			case None => 128;
			case Some(value) => value;
		}
		
		val nbSheep: Int = Try(arguments("nbSheeps").toInt).toOption match {
			case None => 100;
			case Some(value) => value;
		}
		
		val nbWolves: Int = Try(arguments("nbWolves").toInt).toOption match {
			case None => 100;
			case Some(value) => value;
		}
		
		val runtime = Runtime.getRuntime;
		
		val ramInit = runtime.totalMemory - runtime.freeMemory;
		Console.println("** Used Memory:  " + ramInit);
		
		def processFactory(animal: Animal): AnimalSimple = {
			new AnimalSimpleImp(animal);
		}
		
		val sheepFactory: (((Int, Int)) => Sheep) = Main.loadSheepFactory(arguments);
		
		val wolfFactory: (((Int, Int)) => Wolf) = Main.loadWolfFactory(arguments);
		
		val map = new MapImp[AnimalSimple](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile(None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalSimple](), processFactory, wolfFactory, sheepFactory, 0, 0);
		
		for(i <- 0 to nbSheep) {
			val sheep = sheepFactory(0, 0);
			val animalProcess = processFactory(sheep);
			map.animalProcesses += animalProcess;
		}
		
		for(i <- 0 to nbWolves) {
			val wolf = wolfFactory(0, 0);
			val animalProcess = processFactory(wolf);
			map.animalProcesses += animalProcess;
		}
		
		val ramEnd = runtime.totalMemory - runtime.freeMemory;
		Console.println("** Used Memory: " + ramEnd);
		Console.println("\t" + (ramEnd - ramInit));
	}
}
