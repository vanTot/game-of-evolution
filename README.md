# Game of Evolution

## Installation

- Download the repository (and extract it).
- You need a working Java environment (at least 1.7); you can check yours with "java -version".
- Go into the directory and execute "./gradlew installDist" (if you are using Windows there is the gradlew.bat script for you).
- Now the Game of Evolution is installed in build/install/game-of-evolution, you can run it with "build/install/game-of-evolution/bin/game-of-evolution".

## Execution

The program runs in two different modes:
- Evolution (by default), which simulate a universe with sheeps and wolves.
- Benchmark (when "benchmark" is given as first argument), which is used to know the computation time of the simulations.

### Evolution

You can enter various commands during the execution:
- the empty command to toggle live information.
- "stop" to quit.
- "sl" to follow a random sheep until its death.
- "wl" to follow a random wolf until its death.

### Benchmark

[More on that later.]

## Wiki

Don't forget to take a look at the [wiki](https://gitlab.com/vanTot/game-of-evolution/wikis/home).
Everything is (or will be) explained there, in particular the options.

## Blog

I have opened [a blog](https://gameofevolution.wordpress.com/) to discuss about my/our findings is the Game of Evolution. I hope I'll see you there.

## License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *
* * *

> There's not much point creating the most marvelous children in history, only to find that some awful mammalian instinct drives us to strangle them at birth.

"Oracle", *Crystal Nights and other stories* by Greg Egan
